import { getUserInfo } from "../../helpers/auth-helpers";
import { getBusinesses } from "../../fetch/Business";

export const addUserInfo = () => {
  return async (dispatch) => {
    // const userInfo = await fetch(
    //   `${process.env.REACT_APP_API_URL}/api/currentUser`,
    //   {
    //     method: "POST",
    //     body: JSON.stringify({
    //       userID
    //     }),
    //     headers: {
    //       "Content-Type": "application/json"
    //     }
    //   }
    // );

    // const userInfoData = await userInfo.json();

    dispatch({
      type: "ADD_USER",
      payload: JSON.parse(getUserInfo())
    });
  };
};

export const deleterUserInfo = () => {
  return async (dispatch) => {
    dispatch({
      type: "DELETE_USER_INFO",
      payload: {}
    });
  };
};

export const allBusinesses = async () => {
  const allBusinesses = await getBusinesses();
  return async (dispatch) => {
    dispatch({
      type: "ALL_BUSINESSES",
      payload: allBusinesses
    });
  };
};

export const addRouteOrdersDeliveries = (deliveries) => {
  return (dispatch) => {
    dispatch({
      type: "ADD_DELIVERIES",
      payload: deliveries
    });
  };
};

export const addFees = (fees) => {
  return (dispatch) => {
    dispatch({
      type: "ADD_FEES",
      payload: fees
    });
  };
};

// export const getUserInfo = () => {
//   return (dispatch) => {
//     dispatch({
//       type: "GET_USER_INFO",

//     });
//   };
// };
