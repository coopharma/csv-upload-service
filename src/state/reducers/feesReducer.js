const reducer = (
  state = {
    fuel_surcharge: 0.0
  },
  action
) => {
  switch (action.type) {
    case "ADD_FEES":
      return {
        ...state,
        ...action.payload
      };
    default:
      return state;
  }
};

export default reducer;
