const reducer = (
  state = {
    email: "",
    businessName: "",
    userID: "",
    token: "",
    isAdmin: "",
    customer_id: "",
    firstName: "",
    lastName: ""
  },
  action
) => {
  switch (action.type) {
    case "ADD_USER":
      return {
        ...state,
        ...action.payload
      };
    case "DELETE_USER_INFO":
      return {
        email: "",
        businessName: "",
        userID: "",
        token: "",
        isAdmin: "",
        customer_id: "",
        firstName: "",
        lastName: ""
      };
    case "BUSINESSES":
      return {
        businessName: ""
      };
    default:
      return state;
  }
};

export default reducer;
