import { combineReducers } from "redux";
import userAccountReducer from "./userAccountReducer";
import userRouteOrders from "./userRouteOrders";
import feesReducer from "./feesReducer";

const reducers = combineReducers({
  userAccount: userAccountReducer,
  routeOrders: userRouteOrders,
  fees: feesReducer
});

export default reducers;
