const reducer = (
  //   state = {
  //     address: "",
  //     barcode: "",
  //     batch_number: "",
  //     business_name: "",
  //     city_name: "",
  //     country_name: "",
  //     customer_email: "",
  //     dropoff_date: "",
  //     latitude: "",
  //     longitude: "",
  //     note: "",
  //     order_id: "",
  //     package_quantity: "",
  //     phone: "",
  //     pickup_date: "",
  //     service: "",
  //     status_name: "",
  //     tracking_link: ""
  //   },
  state = [],
  action
) => {
  switch (action.type) {
    case "ADD_DELIVERIES":
      return [...action.payload];
    case "DELETE_DELIVERIES":
      return {
        address: "",
        barcode: "",
        batch_number: "",
        business_name: "",
        city_name: "",
        country_name: "",
        customer_email: "",
        dropoff_date: "",
        latitude: "",
        longitude: "",
        note: "",
        order_id: "",
        package_quantity: "",
        phone: "",
        pickup_date: "",
        service: "",
        status_name: "",
        tracking_link: ""
      };
    default:
      return state;
  }
};

export default reducer;
