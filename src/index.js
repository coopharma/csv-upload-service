import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import "./index.css";
import App from "./App";
import moment from "moment";
import { monthsES, weeksES } from "./helpers/util";

import "bootstrap/dist/css/bootstrap.min.css";
import "mapbox-gl/dist/mapbox-gl.css";

import { Provider } from "react-redux";
import { store } from "./state/store";

moment.updateLocale("es", {
  months: monthsES,
  weekdays: weeksES
});

moment.locale("es");

ReactDOM.render(
  <BrowserRouter>
    <React.StrictMode>
      <Provider store={store}>
        <App />
      </Provider>
    </React.StrictMode>
  </BrowserRouter>,
  document.getElementById("root")
);
