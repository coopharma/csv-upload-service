import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";

import React, { useEffect, useState } from "react";
import { Badge, Image } from "react-bootstrap";

import { Routes, Route, useNavigate } from "react-router-dom";
import { getToken } from "./helpers/auth-helpers";

// Redux
import { useSelector, useDispatch } from "react-redux";
import { bindActionCreators } from "redux";
import { actionCreators } from "./state";

// Components
import AuthTabs from "./containers/Auth";
import PrivateRoute from "./containers/Private/PrivateRoute";
import Home from "./containers/Home";
import TopBar from "./components/TopBar";
import ActiveRoutes from "./containers/Active Routes/index.js";
import SideBar from "./components/Sidebar";
import EstimateCalculator from "./components/Estimate/EstimateCalculator.js";
import LogoLoader from "./components/LogoLoader";
import UserList from "./components/Lists/UserList";
import Footer from "./components/Footer";
import PendingRoutes from "./containers/Pending Routes";
import AddPaymentMethod from "./components/AddPaymentMethod";
import UserMenuButton from "./components/UserMenuButton";
import Billing from "./containers/Billing";
import AddUser from "./containers/AddUser";
import Error from "./containers/Error";

import underConstructionImg from "./images/under_construction.png";
import RouteSettings from "./containers/Route Settings";
import { getFees } from "./fetch/Fees";

const App = () => {
  const navigate = useNavigate();
  const [pageLoader, setPageLoader] = useState(false);
  const { userAccount } = useSelector((state) => state);

  const dispatch = useDispatch();

  const { addUserInfo, addFees } = bindActionCreators(actionCreators, dispatch);

  useEffect(async () => {
    if (!getToken()) {
      return navigate("/auth", { replace: true });
    }

    init();

    // Add fees to global state
    const feesResponseData = await getFees();

    addFees(feesResponseData);

    setPageLoader(true);
    addUserInfo();
    setTimeout(() => {
      setPageLoader(false);
    }, 2000);
  }, []);

  const init = () => {
    window.initHippo({
      appSecretKey: "4e1a82dad7a343b1825806803dd17a02",
      language: "en",
      tags: ["Client", "Registered"],
      botGroupID: 2644
    });
  };

  return (
    <>
      {pageLoader ? (
        <LogoLoader />
      ) : (
        <>
          <div>
            <div className="App m-4" style={{ position: "relative" }}>
              <div
                style={{
                  position: "absolute",
                  right: 0,
                  zIndex: "999",
                  textAlign: "right"
                }}
              >
                <UserMenuButton />
                {/* <h4 className="m-1">{userAccount.businessName}</h4> */}
                {userAccount.isAdmin && (
                  <>
                    <small>
                      <Badge color="primary">Admin</Badge>
                    </small>
                  </>
                )}
              </div>
              <TopBar />
              <SideBar />
              <Routes>
                <Route
                  path="/"
                  element={<Home isAdmin={userAccount.isAdmin} />}
                />
                <Route path="active-routes" element={<ActiveRoutes />} />
                <Route path="pending-routes" element={<PendingRoutes />} />
                <Route path="get-estimate" element={<EstimateCalculator />} />

                <Route
                  path="user-list"
                  element={
                    <PrivateRoute
                      Component={() => (
                        <UserList
                          listHeaders={[
                            "Name",
                            "Username",
                            "Email",
                            "Phone",
                            "Address",
                            "Business",
                            "Admin"
                          ]}
                          listData={[]}
                        />
                      )}
                    />
                  }
                />
                <Route
                  path="user-list/add-user"
                  element={<PrivateRoute Component={() => <AddUser />} />}
                />
                <Route path="auth" element={<AuthTabs />} />
                <Route
                  path="add-payment-method"
                  element={<AddPaymentMethod />}
                />
                <Route path="billing" element={<Billing />} />
                <Route
                  path="profile"
                  element={
                    <div className="text-center mt-4">
                      {/* <h1>Under construction</h1> */}
                      <Image src={underConstructionImg} width="400" />
                    </div>
                  }
                />
                <Route
                  path="route-settings"
                  element={<PrivateRoute Component={() => <RouteSettings />} />}
                />
                <Route
                  path="error-view"
                  element={<PrivateRoute Component={() => <Error />} />}
                />
                {/* <Route path="dashboard" element={<Table />} /> */}
                <Route path="*" element={<p>Nothing to see</p>} />
              </Routes>
              <div style={{ height: "50px" }}></div>
            </div>
          </div>
          <Footer />
        </>
      )}
    </>
  );
};

export default App;
