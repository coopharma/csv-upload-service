import React from "react";
import { Image } from "react-bootstrap";
import flexioLogo from "../../images/Flexio_color.png";

const TopBar = () => {
  return (
    <div className="d-flex justify-content-center">
      <Image src={flexioLogo} width="200" fluid />
    </div>
  );
};

export default TopBar;
