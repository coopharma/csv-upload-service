import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import StripeUI from "../../containers/Stripe";

const AddPaymentMethod = ({ clientSecret, addPaymentMethod }) => {
  return (
    <Container>
      <Row>
        <Col md="12" style={{ margin: "2em auto 0 auto" }}>
          <StripeUI
            clientSecret={clientSecret}
            onIntentCreated={addPaymentMethod}
            buttonText="Finish"
          />
        </Col>
      </Row>
    </Container>
  );
};

export default AddPaymentMethod;
