import React from "react";
import { Card, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";

import { useSelector } from "react-redux";

const Drawer = ({ open, hideDrawer }) => {
  const { userAccount } = useSelector((state) => state);

  return (
    <div className={`${open ? "open-drawer" : ""} drawer-menu`}>
      <Card className="mt-1">
        <Card.Body className=" text-center">
          <div className="mb-3">
            <small>Business</small>
            <div>
              <h3>{userAccount.businessName}</h3>
            </div>
            <hr />
            <div>
              {userAccount.firstName} {userAccount.lastName}
            </div>
            <small>{userAccount.email}</small>
          </div>
          <hr />
          <Nav className="d-block">
            <Nav.Item>
              <Link
                to="profile"
                className="sidebar-link d-block"
                onClick={hideDrawer}
              >
                Profile
              </Link>
            </Nav.Item>
            <Nav.Item>
              <Link
                to="billing"
                className="sidebar-link mt-2 d-block"
                onClick={hideDrawer}
              >
                Billing
              </Link>
            </Nav.Item>
            <Nav.Item hidden={!userAccount.isAdmin}>
              <Link
                to="route-settings"
                className="sidebar-link mt-2 d-block"
                onClick={hideDrawer}
              >
                Route Settings
              </Link>
            </Nav.Item>
          </Nav>
        </Card.Body>
      </Card>
    </div>
  );
};

export default Drawer;
