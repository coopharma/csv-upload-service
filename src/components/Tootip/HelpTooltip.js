import React from "react";
import { Button, OverlayTrigger, Tooltip } from "react-bootstrap";

const styleDefault = {
  background: "rgba(0,0,0,0)",
  color: "#000",
  borderRadius: "50%",
  display: "inline-block",
  margin: "0 10px",
  width: "30px",
  height: "30px",
  textAlign: "center",
  verticalAlign: "middle",
  padding: 0
};

const HelpTooltip = ({ tooltipText, style = styleDefault }) => {
  return (
    <OverlayTrigger
      placement="right"
      overlay={<Tooltip id="tooltip-right">{tooltipText}</Tooltip>}
    >
      <Button variant="secondary" style={style}>
        ?
      </Button>
    </OverlayTrigger>
  );
};

export default HelpTooltip;
