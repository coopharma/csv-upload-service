import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Form, Button, Spinner } from "react-bootstrap";

import { setToken, setUserInfo } from "../../helpers/auth-helpers";
import { register } from "../../fetch/Auth";
import AddPaymentMethod from "../AddPaymentMethod";
import { getClientSecret } from "../../fetch/Stripe";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import Loader from "../Loader";

// Redux
import { useDispatch } from "react-redux";
import { bindActionCreators } from "redux";
import { actionCreators } from "../../state";
import { useSelector } from "react-redux";

const Register = ({ title }) => {
  const navigate = useNavigate();
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const [loading, setLoading] = useState(false);
  const [stripeClientSecret, setStripeClientSecret] = useState("");
  const [userData, setUserData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    confirmPassword: "",
    phone: "",
    address: "",
    businessName: "",
    salesPerson: "",
    businessType: "",
    newUserAddPaymentMethod: true
  });
  const { userAccount } = useSelector((state) => state);

  const dispatch = useDispatch();
  const { addUserInfo } = bindActionCreators(actionCreators, dispatch);

  return (
    <>
      {!stripeClientSecret ? (
        <>
          <div className="p-1 text-center mt-2">
            <h2>{title}</h2>
          </div>
          <Form onSubmit={(e) => handleSubmit(e)}>
            <Form.Group className="mt-2">
              <Form.Label>
                Name <small className="text-danger"> *</small>
              </Form.Label>
              <Form.Control
                type="text"
                name="firstName"
                id="firstName"
                placeholder="Name"
                value={userData.firstName}
                onChange={handleChange}
                required
              />
            </Form.Group>
            <Form.Group className="mt-2">
              <Form.Label>
                Last Name <small className="text-danger"> *</small>
              </Form.Label>
              <Form.Control
                type="text"
                name="lastName"
                id="lastName"
                placeholder="Last Name"
                value={userData.lastName}
                onChange={handleChange}
                required
              />
            </Form.Group>
            <Form.Group className="mt-2">
              <Form.Label>
                Address <small className="text-danger"> *</small>
              </Form.Label>
              <Form.Control
                type="text"
                name="address"
                id="address"
                placeholder="Address"
                value={userData.address}
                onChange={handleChange}
                required
              />
            </Form.Group>
            <Form.Group className="mt-2">
              <Form.Label>
                Phone <small className="text-danger"> *</small>
              </Form.Label>
              <Form.Control
                type="tel"
                name="phone"
                id="phone"
                placeholder="7871234567"
                value={userData.phone}
                onChange={(e) => {
                  if (isNaN(e.target.value)) return;
                  handleChange(e);
                }}
                maxLength="10"
                required
              />
            </Form.Group>
            <Form.Group className="mt-2">
              <Form.Label>
                Business Name <small className="text-danger"> *</small>
              </Form.Label>
              <Form.Control
                type="text"
                name="businessName"
                id="businessName"
                placeholder="Business Name"
                value={userData.businessName}
                onChange={handleChange}
                required
              />
            </Form.Group>
            <Form.Group className="mt-2">
              <Form.Label>Business Type</Form.Label>
              <Form.Select
                onChange={handleChange}
                name="businessType"
                id="businessType"
                value={userData.businessType}
              >
                <option value="">Select an option</option>
                <option value="B2B">B2B</option>
                <option value="B2C">B2C</option>
              </Form.Select>
            </Form.Group>
            <Form.Group className="mt-2">
              <Form.Label>Sales Person</Form.Label>
              <Form.Control
                type="text"
                name="salesPerson"
                id="salesPerson"
                placeholder="Sales Person"
                value={userData.salesPerson}
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group className="mt-2">
              <Form.Label>
                Email <small className="text-danger"> *</small>
              </Form.Label>
              <Form.Control
                type="email"
                name="email"
                id="registerEmail"
                placeholder="Email"
                value={userData.email}
                onChange={handleChange}
                required
              />
            </Form.Group>
            <Form.Group className="mt-2">
              <Form.Label>
                Password <small className="text-danger"> *</small>
              </Form.Label>
              <Form.Control
                type="password"
                name="password"
                id="registerPassword"
                placeholder="Password"
                value={userData.password}
                onChange={handleChange}
                required
              />
            </Form.Group>
            <Form.Group className="mt-2">
              <Form.Label>
                Confirm Password <small className="text-danger"> *</small>
              </Form.Label>
              <Form.Control
                type="password"
                name="confirmPassword"
                id="confirmPassword"
                placeholder="Confirm Password"
                value={userData.confirmPassword}
                onChange={handleChange}
                required
              />
            </Form.Group>
            {userAccount.isAdmin && (
              <Form.Group className="mt-2">
                <Form.Label>
                  Add Payment Method<small className="text-danger"> *</small>
                </Form.Label>
                {/* <Form.Check
                  type="switch"
                  onChange={handleChange}
                  checked={userData.newUserAddPaymentMethod}
                  name="newUserAddPaymentMethod"
                /> */}
                <Form.Switch
                  onChange={handleChangeSwitch}
                  checked={userData.newUserAddPaymentMethod}
                  name="newUserAddPaymentMethod"
                />
              </Form.Group>
            )}
            {error && <p className="text-danger font-weight-bold">{error}</p>}
            {success && (
              <p className="text-success font-weight-bold">{success}</p>
            )}
            <div className="d-flex align-items-center">
              <Button
                color="primary"
                type="submit"
                className="mt-2"
                disabled={loading}
              >
                {userData.newUserAddPaymentMethod ? (
                  <>
                    Next Step <FontAwesomeIcon icon={faArrowRight} />
                  </>
                ) : (
                  "Submit"
                )}
              </Button>
              {loading && (
                <Spinner
                  size="md"
                  animation="border"
                  style={{ marginLeft: "5px" }}
                />
              )}
            </div>
          </Form>
        </>
      ) : (
        <>
          <div className="p-1 text-center mt-2">
            <h2>Add Payment Method</h2>
          </div>
          <AddPaymentMethod
            clientSecret={stripeClientSecret}
            addPaymentMethod={addPaymentMethod}
          />
          {loading && <Loader />}
          {success}
        </>
      )}
    </>
  );

  async function handleSubmit(e) {
    e.preventDefault();

    setLoading(true);

    if (userData.password != userData.confirmPassword) {
      setLoading(false);
      return setError("Password is not the same.");
    }

    const registerResult = await register(userData);

    if (registerResult.error) {
      setLoading(false);
      return setError(registerResult.error);
    } else {
      if (!userData.newUserAddPaymentMethod) {
        setSuccess(`You will be redirected...`);

        if (!userAccount.isAdmin) {
          setTimeout(() => {
            window.location.href = "/auth";
          }, 3000);
        } else {
          setTimeout(() => {
            window.location.href = "/";
          }, 3000);
        }

        return;
      }

      setToken(registerResult.token);
      setUserInfo(JSON.stringify(registerResult));
      addUserInfo();

      const clientSecretResponse = await getClientSecret(
        registerResult.customer_id,
        ""
      );
      setStripeClientSecret(clientSecretResponse.client_secret);
      setLoading(false);
    }
  }

  async function addPaymentMethod() {
    setLoading(true);
    setSuccess(`You will be redirected...`);

    setTimeout(() => {
      window.location.href = "/";
      return;
    }, 3000);
  }

  function handleChange(e) {
    const { name, value } = e.target;

    setUserData({
      ...userData,
      [name]: value
    });
  }

  function handleChangeSwitch(e) {
    setUserData({
      ...userData,
      newUserAddPaymentMethod: !userData.newUserAddPaymentMethod
    });
  }
};

export default Register;
