import React, { useState, useCallback } from "react";
import { useNavigate } from "react-router-dom";
import flexioLogo from "../../images/Flexio_color.png";
import { Nav, NavDropdown } from "react-bootstrap";
import { Link } from "react-router-dom";
import {
  deleteToken,
  getToken,
  deleteCurrentUserInfo
} from "../../helpers/auth-helpers";

import { bindActionCreators } from "redux";
import { useDispatch, useSelector } from "react-redux";
import { actionCreators } from "../../state";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRightFromBracket } from "@fortawesome/free-solid-svg-icons";

const Sidebar = () => {
  const [isToggled, setIsToggled] = useToggle(false);
  let navigate = useNavigate();
  const dispatch = useDispatch();
  const { userAccount } = useSelector((state) => state);

  const { deleterUserInfo } = bindActionCreators(actionCreators, dispatch);

  const logout = () => {
    deleteToken();
    deleterUserInfo();
    deleteCurrentUserInfo();
    return navigate("/auth", { replace: true });
  };

  function useToggle(initialState = false) {
    const [state, setState] = useState(initialState);

    const toggle = useCallback(() => {
      setState((state) => !state, []);
    });

    return [state, toggle];
  }

  return (
    <>
      {getToken() && (
        <>
          {isToggled && <div className="overlay" onClick={setIsToggled}></div>}
          <div className="hamburger-menu fixed-top" onClick={setIsToggled}>
            <div className="hamburger-menu_item"></div>
            <div className="hamburger-menu_item"></div>
            <div className="hamburger-menu_item"></div>
            <div className="menu-text-hidden">MENU</div>
          </div>
          <div className={`sidebar ${isToggled ? "" : "hide-sidebar"}`}>
            {/* <div className="hamburger-menu" onClick={setIsToggled}>
            <div className="hamburger-menu_item"></div>
            <div className="hamburger-menu_item"></div>
            <div className="hamburger-menu_item"></div>
            </div> */}
            {/* <Link to="/" className="d-block text-center"></Link> */}
            {/* <hr /> */}
            <Nav
              className="d-block"
              style={{
                marginTop: "120px",
                position: "relative",
                height: "100vh"
              }}
            >
              <Nav.Item>
                <Link
                  to="/"
                  className="nav-link sidebar-link font-weight-light"
                  onClick={setIsToggled}
                >
                  Create Route
                </Link>
              </Nav.Item>
              <Nav.Item>
                <NavDropdown
                  title="Routes"
                  className="nav-link sidebar-link font-weight-light"
                >
                  <Link
                    to="active-routes"
                    className="nav-link sidebar-link font-weight-light"
                    onClick={setIsToggled}
                  >
                    Active Routes
                  </Link>

                  <Link
                    to="pending-routes"
                    className="nav-link sidebar-link font-weight-light"
                    onClick={setIsToggled}
                  >
                    Pending Confirmation Routes
                  </Link>
                </NavDropdown>
              </Nav.Item>
              <Nav.Item>
                <Link
                  to="get-estimate"
                  className="nav-link sidebar-link font-weight-light"
                  onClick={setIsToggled}
                >
                  Get Estimate
                </Link>
              </Nav.Item>
              {userAccount.isAdmin && (
                <Nav.Item>
                  <Link
                    to="user-list"
                    className="nav-link sidebar-link font-weight-light"
                    onClick={setIsToggled}
                  >
                    Users
                  </Link>
                </Nav.Item>
              )}
              {userAccount.isAdmin && (
                <Nav.Item>
                  <Link
                    to="error-view"
                    className="nav-link sidebar-link font-weight-light"
                    onClick={setIsToggled}
                  >
                    Errors List
                  </Link>
                </Nav.Item>
              )}
              <Nav.Item
                style={{ position: "absolute", width: "100%", bottom: "120px" }}
              >
                <Nav.Link
                  to="#"
                  replace="true"
                  className="nav-link sidebar-link font-weight-light"
                  onClick={() => {
                    setIsToggled();
                    logout();
                  }}
                >
                  Logout <FontAwesomeIcon icon={faArrowRightFromBracket} />
                </Nav.Link>
              </Nav.Item>
            </Nav>
          </div>
        </>
      )}
    </>
  );
};

export default Sidebar;
