import React, { useState, useRef } from "react";
import {
  Form,
  Button,
  Card,
  Alert,
  Row,
  Col,
  Container
} from "react-bootstrap";
import { useSelector } from "react-redux";
import { flexioRates, truckMilesPrice } from "../../helpers/util";
import HelpTooltip from "../Tootip/HelpTooltip";

const EstimateCalculator = () => {
  const [alertVariant, setAlertVariant] = useState("");
  const [showFields, setShowFields] = useState(false);
  const estimateForm = useRef(null);
  const [territory, setTerritory] = useState("");
  const [flexioService, setFlexioService] = useState("");
  const [calcFieldsValue, setCalcFieldsValue] = useState({
    totalRouteTime: "",
    routeDistance: "",
    returnDistance: "",
    overWaitingTime: "",
    deliveryStopsTotal: "",
    overTransitTime: ""
  });
  const [error, setError] = useState("");
  const [estimates, setEstimate] = useState({
    vansXLEstimate: 0,
    vansSMEstimate: 0,
    SUVEstimate: 0,
    carsEstimate: 0,
    truckEstimate: []
  });
  const [showTable, setShowTable] = useState(false);
  const { fuel_surcharge: fuelSurcharge } = useSelector((state) => state.fees);

  const handleChange = (e) => {
    const value = e.target.value;
    const key = e.target.name;

    setCalcFieldsValue({
      ...calcFieldsValue,
      [key]: value
    });
  };

  const notANumber = (value) => {
    if (isNaN(value)) return true;

    return false;
  };

  const submitForm = (e) => {
    e.preventDefault();

    const service = flexioService;

    switch (service) {
      case "Flexio Shift":
        if (!isFieldsFill()) {
          setAlertVariant("danger");
          setError("You need to fill all fields");
          return;
        }
        setError("");
        if (calculateRate()) {
          setShowTable(true);
        } else {
          setShowTable(false);
        }
        break;
      case "Flexio Now":
        if (!isFieldsFill()) {
          setAlertVariant("danger");
          setError("You need to fill all fields");
          return;
        }
        setError("");
        if (calculateRate()) {
          setShowTable(true);
        } else {
          setShowTable(false);
        }
        break;
      case "Trucks":
        if (!isFieldsFill()) {
          setAlertVariant("danger");
          setError("You need to fill all fields");
          return;
        }
        setError("");
        if (calculateRate()) {
          setShowTable(true);
        } else {
          setShowTable(false);
        }
        break;
      default:
        break;
    }
  };

  const isFieldsFill = () => {
    const service = flexioService;
    if (
      service === "Trucks" &&
      (!calcFieldsValue.routeDistance || !calcFieldsValue.deliveryStopsTotal)
    ) {
      return false;
    } else if (
      service === "Flexio Shift" &&
      (!calcFieldsValue.totalRouteTime ||
        !calcFieldsValue.routeDistance ||
        !calcFieldsValue.returnDistance)
    ) {
      return false;
    } else if (
      service === "Flexio Now" &&
      (!calcFieldsValue.totalRouteTime ||
        !calcFieldsValue.routeDistance ||
        !calcFieldsValue.returnDistance)
    ) {
      return false;
    }

    return true;
  };

  const calculateRate = () => {
    const {
      totalRouteTime,
      routeDistance,
      returnDistance,
      deliveryStopsTotal
    } = calcFieldsValue;

    const fleet = flexioRates[territory][flexioService];

    let vansXLEst, vansSMEst, SUVEst, carsEst;

    if (flexioService === "Flexio Shift") {
      vansXLEst = (
        fleet.vansXL.basePrice +
        (totalRouteTime > 3
          ? (totalRouteTime - 3) * fleet.vansXL.pricePerAdditionalHour
          : 0) +
        routeDistance * fleet.vansXL.priceperMileOnRoute +
        (returnDistance >= 20
          ? returnDistance * fleet.vansXL.pricePerReturn
          : 0)
      ).toFixed(2);

      vansSMEst = (
        fleet.vansSM.basePrice +
        (totalRouteTime > 3
          ? (totalRouteTime - 3) * fleet.vansSM.pricePerAdditionalHour
          : 0) +
        routeDistance * fleet.vansSM.priceperMileOnRoute +
        (returnDistance >= 20
          ? returnDistance * fleet.vansSM.pricePerReturn
          : 0)
      ).toFixed(2);

      SUVEst = (
        fleet.SUV.basePrice +
        (totalRouteTime > 3
          ? (totalRouteTime - 3) * fleet.SUV.pricePerAdditionalHour
          : 0) +
        routeDistance * fleet.SUV.priceperMileOnRoute +
        (returnDistance >= 20 ? returnDistance * fleet.SUV.pricePerReturn : 0)
      ).toFixed(2);

      carsEst = (
        fleet.cars.basePrice +
        (totalRouteTime > 3
          ? (totalRouteTime - 3) * fleet.cars.pricePerAdditionalHour
          : 0) +
        routeDistance * fleet.cars.priceperMileOnRoute +
        (returnDistance >= 20 ? returnDistance * fleet.cars.pricePerReturn : 0)
      ).toFixed(2);

      setEstimate({});
      setEstimate({
        vansXLEstimate: vansXLEst,
        vansSMEstimate: vansSMEst,
        SUVEstimate: SUVEst,
        carsEstimate: carsEst
      });

      return true;
    } else if (flexioService === "Flexio Now") {
      SUVEst = (
        fleet.SUV.basePrice +
        routeDistance * fleet.SUV.priceperMileOnRoute +
        returnDistance * fleet.SUV.pricePerReturn
      ).toFixed(2);

      carsEst = (
        fleet.cars.basePrice +
        routeDistance * fleet.cars.priceperMileOnRoute +
        returnDistance * fleet.cars.pricePerReturn
      ).toFixed(2);

      setEstimate({});
      setEstimate({
        SUVEstimate: SUVEst,
        carsEstimate: carsEst
      });

      return true;
    } else {
      if (routeDistance <= 0) {
        setAlertVariant("info");
        setError("Milage must be more than 0.");
        return false;
      }

      if (routeDistance > 198) {
        setAlertVariant("info");
        setError("Please call the office for a final estimate.");
        return false;
      }

      const truckType1Est = (
        truckMilesPrice[Math.round(routeDistance)][0] * 1.2 +
        (deliveryStopsTotal > 1 ? (deliveryStopsTotal - 1) * 20 : 0) +
        routeDistance * fuelSurcharge
      ).toFixed(2);

      const truckType2Est = (
        truckMilesPrice[Math.round(routeDistance)][1] * 1.2 +
        (deliveryStopsTotal > 1 ? (deliveryStopsTotal - 1) * 20 : 0) +
        routeDistance * fuelSurcharge
      ).toFixed(2);

      setEstimate({});
      setEstimate({
        truckEstimate: [truckType1Est, truckType2Est]
      });

      return true;
    }
  };

  return (
    <Container>
      <Row>
        <Col md="6" className="m-auto">
          <h2 className="text-center mb-3">Rates Calculator</h2>
          <Form onSubmit={submitForm} className="m-2" ref={estimateForm}>
            <Form.Group className="mt-2">
              <Form.Label className="fw-bold">Territory</Form.Label>
              <Form.Select
                required
                onChange={(e) => {
                  setTerritory(e.target.value);
                  setFlexioService("");
                  setShowFields(false);
                  setShowTable(false);
                }}
                name="territory"
                id="territory"
                value={territory}
              >
                <option value="">Choose a territory</option>
                <option value="Puerto Rico">Puerto Rico</option>
                <option value="Florida">Florida</option>
              </Form.Select>
            </Form.Group>
            {territory && (
              <>
                <Form.Group className="mt-2">
                  <Form.Label className="fw-bold">Service</Form.Label>
                  <Form.Select
                    required
                    onChange={(e) => {
                      setFlexioService(e.target.value);
                      if (!e.target.value.length) {
                        setShowFields(false);
                        setCalcFieldsValue({
                          ...calcFieldsValue,
                          totalRouteTime: "",
                          routeDistance: "",
                          returnDistance: "",
                          overWaitingTime: "",
                          deliveryStopsTotal: "",
                          overTransitTime: ""
                        });
                        setShowTable(false);
                        return;
                      }
                      setShowTable(false);
                      setShowFields(true);
                      handleChange(e);
                    }}
                    name="service"
                    id="service"
                    value={flexioService}
                  >
                    <option value="">Choose a service</option>
                    <option value="Flexio Shift">Flexio Shift</option>
                    {/* <option value="Flexio Now">Flexio Now</option> */}
                    <option
                      value="Trucks"
                      hidden={territory === "Florida" ? true : false}
                    >
                      Trucks
                    </option>
                  </Form.Select>
                </Form.Group>
                {territory && !flexioService && (
                  <div className="m-2">
                    <p>
                      <strong>Flexio Shift</strong> service is a dedicated
                      vehicle service which rates are based on distance and time
                      per route. You can use the whole cargo space of the
                      vehicle for the same price.
                    </p>
                    <p>
                      <strong>Trucks</strong> service is a dedicated Box Truck
                      vehicle service which rates are based on the NTSP
                      (Negociado de Transporte y Servicios Públicos) of PR.
                    </p>
                  </div>
                )}
              </>
            )}
            {showFields && (
              <>
                <Form.Group
                  className="mt-2"
                  hidden={flexioService !== "Trucks" ? false : true}
                >
                  <Form.Label className="fw-bold">
                    Total Route Time (hrs)
                    <HelpTooltip
                      tooltipText="This value must include the total time from arriving to the 
                    pickup location, waiting/service times in each location, and travel time until the last delivery location."
                    />
                  </Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Total Route Time (hrs)"
                    name="totalRouteTime"
                    value={calcFieldsValue.totalRouteTime}
                    onChange={(e) => {
                      if (notANumber(e.target.value)) return;
                      handleChange(e);
                    }}
                  />
                </Form.Group>
                <Form.Group className="mt-2">
                  <Form.Label className="fw-bold">
                    Route Distance (mi)
                    <HelpTooltip tooltipText="This value includes total miles travelled from pickup location to last delivery location." />
                  </Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Route Distance (mi)"
                    name="routeDistance"
                    value={calcFieldsValue.routeDistance}
                    onChange={(e) => {
                      if (notANumber(e.target.value)) return;
                      handleChange(e);
                    }}
                  />
                </Form.Group>
                <Form.Group
                  className="mt-2"
                  hidden={flexioService !== "Trucks" ? false : true}
                >
                  <Form.Label className="fw-bold">
                    Return Distance (mi)
                    <HelpTooltip tooltipText="This value includes the distance travelled from the last delivery location to the pickup location." />
                  </Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Return Distance (mi)"
                    name="returnDistance"
                    value={calcFieldsValue.returnDistance}
                    onChange={(e) => {
                      if (notANumber(e.target.value)) return;
                      handleChange(e);
                    }}
                  />
                </Form.Group>
                {/* <Form.Group
              className="mt-2"
              hidden={calcFieldsValue.service === "Flexio Now" ? false : true}
            >
              <Form.Label className="fw-bold">
                Over Waiting Time (mins)
              </Form.Label>
              <Form.Control
                type="text"
                placeholder="Over Waiting Time (mins)"
                name="overWaitingTime"
                value={calcFieldsValue.overWaitingTime || ""}
                onChange={(e) => {
                  if (notANumber(e.target.value)) return;
                  handleChange(e);
                }}
              />
            </Form.Group> */}
                <Form.Group
                  className="mt-2"
                  hidden={flexioService === "Trucks" ? false : true}
                >
                  <Form.Label className="fw-bold">
                    Delivery Stops Total
                    <HelpTooltip tooltipText="This value considers total number of delivery locations, without adding the pickup location." />
                  </Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Delivery Stops Total"
                    name="deliveryStopsTotal"
                    value={calcFieldsValue.deliveryStopsTotal}
                    onChange={(e) => {
                      if (notANumber(e.target.value)) return;
                      handleChange(e);
                    }}
                  />
                </Form.Group>
                {/* <Form.Group
              className="mt-2"
              hidden={flexioService === "Flexio Now" ? false : true}
            >
              <Form.Label className="fw-bold">
                Over Transit Time (mins)
              </Form.Label>
              <Form.Control
                type="text"
                placeholder="Over Transit Time (mins)"
                name="overTransitTime"
                value={calcFieldsValue.overTransitTime || ""}
                onChange={(e) => {
                  if (notANumber(e.target.value)) return;
                  handleChange(e);
                }}
              />
            </Form.Group> */}
              </>
            )}
            <Button
              type="submit"
              hidden={flexioService ? false : true}
              className="mt-3"
            >
              Get Estimate
            </Button>
            {error && (
              <Alert variant={alertVariant} className="mt-3">
                {error}
              </Alert>
            )}
          </Form>
          {showTable && (
            <>
              <Card className="m-3">
                <Card.Body>
                  <Card.Title className="text-center">
                    <h3>{flexioService}</h3>
                  </Card.Title>
                  <div className="d-flex justify-content-around text-center m-2">
                    {flexioService === "Flexio Shift" && (
                      <>
                        <div>
                          <div className="fw-bold mb-2">Vans XL</div>
                          <div>${estimates.vansXLEstimate}</div>
                        </div>
                        <div>
                          <div className="fw-bold mb-2">Vans SM</div>
                          <div>${estimates.vansSMEstimate}</div>
                        </div>
                      </>
                    )}
                    {(flexioService === "Flexio Now" ||
                      flexioService === "Flexio Shift") && (
                      <>
                        <div>
                          <div className="fw-bold mb-2">SUV</div>
                          <div>${estimates.SUVEstimate}</div>
                        </div>
                        <div>
                          <div className="fw-bold mb-2">Cars</div>
                          <div>${estimates.carsEstimate}</div>
                        </div>
                      </>
                    )}
                    {flexioService === "Trucks" &&
                      estimates.truckEstimate.length > 0 && (
                        <>
                          {estimates.truckEstimate.map(
                            (truckPrice, truckIndex) => {
                              return (
                                <div key={truckIndex}>
                                  <div className="fw-bold mb-2">
                                    Truck Type {truckIndex + 1}
                                  </div>
                                  <div>${truckPrice}</div>
                                </div>
                              );
                            }
                          )}
                        </>
                      )}
                  </div>
                </Card.Body>
              </Card>
              <div className="m-3">
                {flexioService === "Flexio Shift" ? (
                  <>
                    <p>
                      The total estimate considers a dedicated vehicle.
                      Depending the cargo volume and weight, a vehicle type will
                      be assigned to your route.
                    </p>
                    <p>
                      This estimate could vary depending on changes to the route
                      or additional total time of the route.
                    </p>
                  </>
                ) : (
                  <>
                    <p>
                      The total estimate considers a dedicated dry box truck. If
                      your cargo is from 1-6 pallets, Truck Type 1 applies. If 7
                      or more pallets, Truck Type 2 is applied.
                    </p>
                    <p>
                      This estimate could vary depending on changes to the
                      route.
                    </p>
                    <p>Fuel Surcharge is applied in this estimate.</p>
                  </>
                )}
              </div>
            </>
          )}
        </Col>
      </Row>
    </Container>
  );
};

export default EstimateCalculator;
