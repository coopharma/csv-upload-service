import React, { useEffect, useState } from "react";
import SweetAlert from "react-bootstrap-sweetalert";
import { Card } from "react-bootstrap";

const EstimateAlert = ({
  alertInfo,
  showEstimateAlert,
  hideAlert,
  pickupTimes,
  onEstimateAlertConfirm,
  onEstimateAlertCancel,
  showCancelBtn,
  showConfirmBtn,
  cancelBtnText,
  confirmBtnText,
  children
}) => {
  const [routeEstimate, setRouteEstimate] = useState({});

  const addRouteEstimate = (data) => {
    if (data.routeDetails !== undefined) {
      setRouteEstimate({
        ...data.estimates,
        ...data.routeDetails,
        ...data,
        shift_VANSXL_pr: data.estimates["Flexio Shift VansXL PR"],
        shift_VANSSM_pr: data.estimates["Flexio Shift VansSM PR"],
        shift_SUV_pr: data.estimates["Flexio Shift SUV PR"],
        shift_CARS_pr: data.estimates["Flexio Shift Cars PR"],
        now_SUV_pr: data.estimates["Flexio Now SUV PR"],
        now_CARS_pr: data.estimates["Flexio Now CARS PR"],
        trucks_type_1: data.estimates["Flexio Truck Type 1"],
        trucks_type_2: data.estimates["Flexio Truck Type 2"]
      });
    } else {
      setRouteEstimate(data);
    }
  };

  useEffect(() => {
    addRouteEstimate(alertInfo);
  });

  return (
    <>
      <SweetAlert
        title="Route Estimate"
        onConfirm={onEstimateAlertConfirm}
        onCancel={onEstimateAlertCancel}
        cancelBtnText={cancelBtnText}
        confirmBtnText={confirmBtnText}
        showCancel={showCancelBtn}
        showConfirm={showConfirmBtn}
        show={showEstimateAlert}
        style={{ maxWidth: "940px", width: "100%", position: "relative" }}
      >
        {/* <div className="x-button" onClick={hideAlert}>
          X
        </div> */}
        <div className="m-2">
          {/* {!!routeEstimate[0].routeDetails ? ( */}
          <>
            <div className="d-flex justify-content-evenly m-3">
              <div>
                Total Distance: {routeEstimate.routific_total_distance} miles
              </div>
              <div>
                Total Return Distance: {routeEstimate.routific_return_distance}{" "}
                miles
              </div>
              <div>Total Time: {routeEstimate.routific_total_time} hours</div>
            </div>

            <div className="d-flex justify-content-evenly m-3">
              <div>
                Return Total Time: {routeEstimate.routific_return_total_time}{" "}
                hours
              </div>
              <div>Package Quantity: {routeEstimate.package_quantity}</div>

              {routeEstimate.routeDetails !== undefined ? (
                <>
                  <div>Total Delivery Stops: {routeEstimate.deliveries}</div>
                </>
              ) : (
                <>
                  <div>
                    Total Delivery Stops:{" "}
                    {routeEstimate.deliveries
                      ? routeEstimate.deliveries.length - 1
                      : "-"}
                  </div>
                </>
              )}
            </div>
          </>
          {/* ) : (
            <>
              <div className="d-flex justify-content-evenly m-3">
                <div>
                  Total Distance: {routeEstimate.routific_total_distance} miles
                </div>
                <div>
                  Total Return Distance:{" "}
                  {routeEstimate.routific_return_distance} miles
                </div>
                <div>Total Time: {routeEstimate.routific_total_time} hours</div>
              </div>

              <div className="d-flex justify-content-evenly m-3">
                <div>
                  Return Total Time: {routeEstimate.routific_return_total_time}{" "}
                  hours
                </div>
                <div>Package Quantity: {routeEstimate.package_quantity}</div>
                <div>
                  Total Delivery Stops: {routeEstimate.deliveries.length - 1}
                </div>
              </div>
            </> */}
          {/* )} */}

          <div className="d-flex justify-content-evenly m-3">
            <div>Pickup Arrived Time: {pickupTimes.pickupArrivedTime}</div>
            <div>
              Last Delivery Completed Time:{" "}
              {pickupTimes.lastDeliveryCompletedTime}
            </div>
          </div>

          <hr />
          <Card className="m-3 flex-grow-1">
            <Card.Body>
              <Card.Title className="text-center mb-4">
                <h4>Flexio Shift</h4>
              </Card.Title>

              <div className="d-flex justify-content-evenly text-center">
                <div>
                  <div className="fw-bold mb-2">Vans XL</div>
                  <div>${routeEstimate.shift_VANSXL_pr}</div>
                </div>
                <div>
                  <div className="fw-bold mb-2">Vans SM</div>
                  <div>${routeEstimate.shift_VANSSM_pr}</div>
                </div>
                <div>
                  <div className="fw-bold mb-2">SUV</div>
                  <div>${routeEstimate.shift_SUV_pr}</div>
                </div>
                <div>
                  <div className="fw-bold mb-2">Cars</div>
                  <div>${routeEstimate.shift_CARS_pr}</div>
                </div>
              </div>
            </Card.Body>
          </Card>
          {/* <Card className="m-3 flex-grow-1">
            <Card.Body>
              <Card.Title className="text-center mb-4">
                <h4>Flexio Now</h4>
              </Card.Title>
              <div className="d-flex justify-content-evenly text-center">
                <div>
                  <div className="fw-bold mb-2">SUV</div>
                  <div>${routeEstimate.now_SUV_pr}</div>
                </div>
                <div>
                  <div className="fw-bold mb-2">Cars</div>
                  <div>${routeEstimate.now_CARS_pr}</div>
                </div>
              </div>
            </Card.Body>
          </Card> */}
          <Card className="m-3 flex-grow-1">
            <Card.Body>
              <Card.Title className="text-center mb-4">
                <h4>Trucks</h4>
              </Card.Title>
              <div className="d-flex justify-content-evenly text-center">
                <div>
                  <div className="fw-bold mb-2">Type 1</div>
                  <div>${routeEstimate.trucks_type_1}</div>
                </div>
                <div>
                  <div className="fw-bold mb-2">Type 2</div>
                  <div>${routeEstimate.trucks_type_2}</div>
                </div>
              </div>
            </Card.Body>
          </Card>
          <div style={{ textAlign: "start" }} className="m-4">
            <p>
              <small>
                * The total estimate considers a dedicated vehicle. Depending
                the cargo volume and weight, a vehicle type will be assigned to
                your route. This estimate could vary depending on changes to the
                route or additional total time of the route.
              </small>
            </p>
            <p>
              <small>* Fuel Surcharge is applied in this estimate.</small>
            </p>
          </div>
        </div>
        {children}
      </SweetAlert>
    </>
  );
};

export default EstimateAlert;
