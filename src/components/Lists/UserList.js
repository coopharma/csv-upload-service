import React, { useState, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Form } from "react-bootstrap";

import { updateUserAdmin, getAllUsers } from "../../fetch/User";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAdd } from "@fortawesome/free-solid-svg-icons";

const UserList = ({ listHeaders = [], listData = [] }) => {
  const { isAdmin } = useSelector((state) => state.userAccount);
  const { email: currentUserEmail } = useSelector((state) => state.userAccount);
  const [usersData, setUsersdata] = useState([]);
  const [loading, setLoading] = useState(false);
  const userContainer = useRef(true);

  const handleUserAdminRole = (id) => {
    setLoading(true);
    const userArray = usersData;

    const userIndex = usersData.findIndex((user) => user.user_id === id);
    userArray[userIndex].is_admin = !userArray[userIndex].is_admin;

    setTimeout(() => {
      updateUserAdmin(id, userArray[userIndex].is_admin);
      setUsersdata(userArray);
      setLoading(false);
    }, 1000);
  };

  useEffect(async () => {
    if (userContainer.current) {
      setLoading(true);
      setUsersdata(await getAllUsers());
      setLoading(false);
    }

    return () => false;
  }, []);

  return (
    <div ref={userContainer}>
      <div className="mb-4">
        <h2>
          All Users
          <Link to="/user-list/add-user" className="m-2 cta-button">
            <FontAwesomeIcon icon={faAdd} size="1x" />
            Add User
          </Link>
        </h2>
      </div>
      <div className="list">
        {listHeaders.map((listHeader, index) => (
          <div
            key={`${listHeader}_index`}
            className="list--list_item list-header fw-bold"
          >
            {listHeader}
          </div>
        ))}
      </div>
      <div>
        {usersData.map((user, index) => {
          if (currentUserEmail === user.email) return;

          return (
            <div
              key={`${user.usernam}_${index}`}
              className="list list-header m-2"
            >
              <div className="list--list_item">
                {`${user.first_name} ${user.last_name}`}
              </div>
              <div className="list--list_item">{user.username}</div>
              <div className="list--list_item">{user.email}</div>
              <div className="list--list_item">{user.phone}</div>
              <div className="list--list_item">{user.address}</div>
              <div className="list--list_item">{user.business_name}</div>
              <div className="list--list_item">
                <Form.Check
                  type="switch"
                  id={`custom-switch-${index}`}
                  //   value={usersData[index].is_admin}
                  onChange={(e) => handleUserAdminRole(user.user_id)}
                  checked={usersData[index].is_admin}
                  disable={`${!loading}`}
                />
              </div>
            </div>
          );
        })}
        <div>{loading && <div className="text-center">Loading...</div>}</div>
      </div>
    </div>
  );
};

export default UserList;
