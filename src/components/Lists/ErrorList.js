import { Table } from "react-bootstrap";
import moment from "moment";

const ErrorList = ({ errorData, headerData }) => {
  return (
    <div>
      <Table responsive>
        <thead>
          <tr>
            {headerData.map((headerItem, headerIndex) => {
              return <th key={`header-${headerIndex + 1}`}>{headerItem}</th>;
            })}
          </tr>
        </thead>
        <tbody>
          {errorData.map((item) => {
            return (
              <tr key={`error-id-${item.errorId}`}>
                <td>{item.errorName}</td>
                <td>{item.errorDescription}</td>
                <td>{moment(new Date(item.errorDate)).format("LLLL")}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  );
};

export default ErrorList;
