import { useState, useEffect } from "react";
import moment from "moment";
import { Badge, Card } from "react-bootstrap";
import { changeStatusColor } from "../../helpers/util";
import SweetAlert from "react-bootstrap-sweetalert";

const SimpleList = ({ listHeaders, listData, className, listStyle }) => {
  useEffect(() => {
    listData = listData.map((item) => item.shift_vans_pr != null);
  }, []);

  return (
    <div className={`list-container ${className}`} style={listStyle}>
      <div className="list">
        {listHeaders.map((theader, headerIndex) => {
          return (
            <div
              className="list--list_item list-header fw-bold"
              key={`list-header-${headerIndex + 1}`}
              id={`list-header-${headerIndex + 1}`}
            >
              {theader}
            </div>
          );
        })}
      </div>
      {listData.map((delivery) => {
        const badgeColor = changeStatusColor(delivery.status_name);

        return (
          <div className="list" key={`list_${delivery.order_id}`}>
            <div className="list--list_item"></div>
            <div className="list--list_item" key={`td-${delivery.order_id}1`}>
              {delivery.business_name}
            </div>
            <div className="list--list_item" key={`td-${delivery.order_id}2`}>
              {delivery.batch_number || "No route number"}
            </div>
            <div className="list--list_item" key={`td-${delivery.order_id}3`}>
              {delivery.service || "No Service"}
            </div>
            <div className="list--list_item" key={`td-${delivery.order_id}33`}>
              {delivery.barcode}
            </div>
            <div className="list--list_item" key={`td-${delivery.order_id}5`}>
              {delivery.phone}
            </div>
            <div className="list--list_item" key={`td-${delivery.order_id}6`}>
              {delivery.customer_email}
            </div>
            <div className="list--list_item" key={`td-${delivery.order_id}7`}>
              {delivery.address}
            </div>
            <div className="list--list_item" key={`td-${delivery.order_id}8`}>
              {delivery.city_name}
            </div>
            <div className="list--list_item" key={`td-${delivery.order_id}9`}>
              {"USA"}
            </div>
            <div className="list--list_item" key={`td-${delivery.order_id}10`}>
              {delivery.country_name}
            </div>
            <div className="list--list_item" key={`td-${delivery.order_id}11`}>
              {delivery.latitude}
            </div>
            <div className="list--list_item" key={`td-${delivery.order_id}12`}>
              {delivery.longitude}
            </div>
            <div className="list--list_item" key={`td-${delivery.order_id}13`}>
              {delivery.note}
            </div>
            <div className="list--list_item" key={`td-${delivery.order_id}14`}>
              {delivery.package_quantity}
            </div>
            <div className="list--list_item" key={`td-${delivery.order_id}17`}>
              {moment(new Date(delivery.pickup_date)).format("LLLL") ||
                "No Pickup Date"}
            </div>
            <div className="list--list_item" key={`td-${delivery.order_id}15`}>
              <a href={delivery.tracking_link} target="_blank">
                {delivery.tracking_link
                  ? "Track Order"
                  : "Tracking not available"}
              </a>
            </div>
            <div className="list--list_item" key={`td-${delivery.order_id}16`}>
              <Badge pill bg={badgeColor}>
                {delivery.status_name}
              </Badge>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default SimpleList;
