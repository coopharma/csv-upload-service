import React, { memo, useState, useEffect, useRef } from "react";
import moment from "moment";
import { Badge, Image } from "react-bootstrap";
import { CSVLink, CSVDownload } from "react-csv";

import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import { actionCreators } from "../../state";

import {
  changeStatusColor,
  changePickupStatusAndColor,
  phoneMask
} from "../../helpers/util";

import mapboxgl from "mapbox-gl";
import polyline from "@mapbox/polyline";

import ConfirmationAlert from "../ConfirmationAlert/index.js";
import EstimateAlert from "../Estimate/EstimateAlert.js";

// const CustomToggle = ({ children, eventKey }) => {
//   const customAccordionEvent = useAccordionButton(eventKey, () => {
//     console.log("Accordion");
//   });

//   return <button onClick={customAccordionEvent}>{children}</button>;
// };

const RouteOrderList = ({
  listData = [],
  listHeaders,
  listStyle = {},
  className,
  title = "",
  confirmRoute = false
}) => {
  const [listRouteOrders, setListRouteOrders] = useState([]);
  const [showAlert, setShowAlert] = useState(false);
  const [showConfirmationAlert, setShowConfirmationAlert] = useState(false);
  const [alertInfo, setAlertInfo] = useState([]);
  const [selectedRoute, setSelectedRoute] = useState([]);
  const [pickupTimes, setPickupTimes] = useState({});
  const mapContainer = useRef(null);
  const map = useRef({});
  const [lng, setLng] = useState(-66.518736);
  const [lat, setLat] = useState(18.246803);
  const [zoom, setZoom] = useState(9);
  const { routeOrders } = useSelector((state) => state);

  mapboxgl.accessToken =
    "pk.eyJ1IjoiZWRpYW5yZXkiLCJhIjoiY2wxMmR1bXN0MDBsYTNibGdjczNvaG9lcSJ9.xT5q6OC2E66mBpWBskccPw";

  const dispatch = useDispatch();

  const { addRouteOrdersDeliveries } = bindActionCreators(
    actionCreators,
    dispatch
  );

  const hideAlert = () => {
    setShowAlert(false);
    // setShowConfirmAlert(false);
    // setShowSuccessAlert(false);
    // setShowConfirmPickupDate(false);
    // setLoading(false);
    // setConfirmPickupDateField(
    //   moment().add(1, "hour").format("yyyy-MM-DDTHH:00")
    // );
  };

  const getPickupTimes = (routeID) => {
    let getPickup = listRouteOrders.filter(
      (order) => order.route_id === routeID
    );

    getPickup = getPickup[0];

    let getDeliveries = getPickup.deliveries.filter(
      (delivery) => delivery.route_id === routeID
    );

    setPickupTimes({
      pickupArrivedTime: !getPickup.arrived_time
        ? "-"
        : moment(new Date(getPickup.arrived_time)).format(
            "MMMM DD YYYY hh:mma"
          ),
      lastDeliveryCompletedTime: !getDeliveries[getDeliveries.length - 1]
        .completed_time
        ? "-"
        : moment(
            new Date(getDeliveries[getDeliveries.length - 1].completed_time)
          ).format("MMMM DD YYYY hh:mma")
    });
  };

  const showEstimate = (routeID) => {
    setShowAlert(true);

    let getEstimates = listRouteOrders.filter(
      (order) => order.route_id === routeID
    );

    getEstimates = [getEstimates.find((order) => order.route_id == routeID)];

    setAlertInfo(getEstimates);
  };

  const navigateToTracking = (trackingLink) => {
    window.open(trackingLink, "_blank");
    return;
  };

  const initRouteMap = () => {
    if (!mapContainer.current) return; // initialize map only once

    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/mapbox/streets-v11",
      center: [lng, lat],
      zoom: zoom
    });

    const poly = polyline.decode(
      "ctxbb@~{a`}Bg@wB{aAb`@gr@fYgJ~CoKfEwLzE{ObGsXnKkCz@cGbBvBbGbQjf@~Mf^bLj~HzY~HzYjH~\nFzY~C~RfEb[~CbVnAnKz@vLvBrXz@zTf@jMf@~Mf@rNRrb@z@fc@z@jz@Rn_@f@nURv[z@bj@nAroAz@~p@nA~sAR~HR~Mf@f^RzEf@vLnAbLz@jMbBjMnAnKbBbLfEjRjCbLnF~RbQ~p@fTvt@jMbe@jCnKfOzh@rNbe@nAzEfE~M~C~MnFbVrD~RrDjWnA~Hf@fEnA~Hf@fEf@bGSbG{@~CcBzE_DfJwG~M{JbQoKfO{EvGwBjCoP~RsDbB_Df@gERoAR{@RcBnA{@bBSnA{@rDcBfEcGzE{EbB_Dz@wBf@oF?wGSkM{@SnFvQnAjHRfESvBg@bBg@~MsInFkCz@SzEjCnAvBf@vGRbe@RrS?rD?rN?~W?~\f@jiAnArXvB~MfJfTrD~Mf@ffAg@~HcGvcAoKzh@cLve@{T~dAwVzuAoAnKz@fEnAfEfO~Wja@ns@vBrDbBbGbBnFRnARnAf@~CRjCRvBgEvVkRrv@kWns@gErIg@Sg@S{@Sg@Rg@RSf@oFgE{@{@c`@k\b`@jz@z@nFfESf@Sz@Rf@Rf@f@f@f@Rf@?z@?f@SRg@R{@?g@?{@Sg@fEsIjWos@jRsv@fEwVSwBSkCg@_DSoASoAcBoFcBcGjz@sl@zc@{YnK_Dju@sSrXoKzTcLv~@kf@zTcLzTwL~C{@~CSnASnA?nAf@vBnArIbLnFbGfJjMnAnAz@RnA?bBg@vB{@jC{@zJwGjf@_Xrq@c`@jCoAnAjCbQf^bVfc@nAjC~HnPbBjCv[nn@jMbVjCzEnAvBvQj\vBzEjCfEzEzJvGbLnAjCbBjCjHbLzOjRbQvQzTbQfJrIzJbLzJrNrDnFrDzEzEnKfE~HjCzEvBzEjHfTvGnU~CvLjCzOrDv[f@~Hz@jRf@rIR~Hg@ni@g@jf@?rDoAjiA?jHSfEg@nd@{@rv@S~SfY?f^?fJf@~Mf@rSnAnUbBrl@R~Hf@nPf@nKf@fJjCfY~CnZbGv[fJzc@bL~a@zaAjhDRjH~Mju@nAvGf@jCfOrl@fJf^f^~nA~M~f@vLjf@vB~MnAbLjCvQzErg@f@bGRjMnAnZg@rXgJzdBcLf}BoA~WcBrl@oAvj@{@ve@?vt@cBr{@{EjgBgEfaAg@~H?nA_Dfw@SfJcBrl@oA~SvGvGRfJnAjM~CvcArXns@~Rn_@vLfsBfr@r{@jWzYzJrS~HjRrIfr@nd@~jW~RbQnPvQjz@vcAb|A~gBzfAzpArNbQrb@ni@zm@zr@r{@zaAriDbeErmBnyBnZve@rXjf@bt@fkAkCvBwVzOos@zh@kCbBkHnFka@fYgOrS{EvLgEvV?v[ka@rDbGr`A_Dbj@~Ccj@cGs`Aja@sD?w[fEwVzEwLfOsSja@gYjHoFjCcBns@{h@vV{OjCwB~CwBct@olAsXwe@c[gc@o_@ce@wmAguAsiDgcEo}@ogAwj@kp@os@s{@{fAsoAo{A_hB{|@gfAgc@sb@crAg|@wQ_IsSsI{uA{c@z@_DSgJcBw[gEgm@kCgc@{@{J_D_I_XkkW_]oFoKwLo_@_Io_@sDoPoA_Sf@gJRkMf@kHf@cGR_D?wB?{E?wBSoFg@kMoAcLSkHS{ERcGRkRRkMz@wGz@{Ef@oAf@oAbBkCfJwLf@sDrSkWbGkHjHoKvGoKrNkWnKoZrDwGni@ksAjHsNjM_NnKwGzc@wVbLkCnd@oKjM_Dvy@wVbLcG?SzJ_IvGcGrIcGz^{Tve@w[vBcBvGoFbj@o_@bVkRrv@on@nAcBjRcQSg^?cGf@{JfEg^jHsv@bB{OjC_Sf@cLRwQ?sX?gJ?{Jg@gOSkCg@cL{@gJcBsIwBgJ{EwLcGwQ{JcV_DgJ{EsNoFcQkCoKwBgJ{EoUoAcG{@_IwBsSkCc[kC{^wBw`@{@wQoAw`@SsX_DsbEoAszCSsaCS__D?glCSgaAS{w@?oA?oi@zEcy@rDce@~Hod@fYg_BvLgm@zO{|@zEsSjCsNfEkWbL_q@~C_IrDoF~C_DrDkCrIkCbQwBrDg@rD{@jCoAjCoAbBcBjRsXbBcBrDkC~CcBnFoAfTg@vVf@~k@f@~Hg@ja@?~u@?zTcBrg@z@zfAvBngAz@nn@z@be@bBni@rDvmA~HjiA~HnqAfJjz@bGjf@~C~nA~HrNR~RRnZoA~_DfTsDfO_DzYsIve@cQzc@cQja@kMv`@sNn_@cLbe@sIvVwB~RoAbQSriDf@v[{@zToA~WwBnd@wGbcAoU~k@gOz^kHr]oFjMg@fc@{@nlAcGjp@{Jb`@cLfEoAv`@oPnlAco@n~B_oArg@{Yb[{OrX{Oja@_Sb[wLnUsIfJkCnUkH~oFfT_DfYsDfc@oAnd@cBvo@kC~p@sDzpAoPju@gJ~u@{JvmAwQvt@gJb`@kCzTSfOf@~Mf@vpBrNnF?rISzJcBnPoFvLkHvGcGfTwVjf@co@rI_I~HoFnU{JvLsDzJwBb[cGzJoArDSvQoAzTSvV?rSf@zTz@rq@~CjHRvL?rNg@bVoAjH{@nUsD~M_DbQ{EfO{Ef^kMz^sNfc@kR~_NzToKfTcLnKkHfJwG~HkHvGkHfOwQnK{OzEgJfE{JbG{OrIgY~Rox@fw@__DvVccAnKsg@rSkdAbBsIrDoUf@wBvGw`@fEwVfOc~@bB{JzJkk@~R{pArDsSbB{JjHgc@fEcQbGc[nK_b@jH_SnKoUnKgTrXcj@fE_IfYsg@f^gm@~k@wcAfTg^vQsXzOsSjRsS~RsSbQoPzO{O~f@_q@vL{TzJcQb[{r@rIcVjHcV~Msl@~C_NvL{h@fO_q@rScy@rI_XbLk\bBoFj{|@rIwQzOs]jH_NnK_SzEsIzY{h@r]gh@nPgTz^gc@jC_Dz^gc@bt@s{@~McQvB_DzToZ~Wgc@vQw[fO_]vVkk@vQ_g@f@cBzJw[rI{YzJce@f@kC~H{^~Hk\bGwVfEcLjCwGnPg^~HkMnFkHzE_InFwGrjAs~AfEcGzEcGrS_XjRkWju@{aAjRwVzJcLvGkHvQ_SvQ_SjRwQbVkRzTcQbL_Izh@_]vt@c`@jk@oUzOwGv[{Jz^{Jfc@{JfYoFnKoAfOwBbQwBfYsDntBcQrq@{Ef_BsNfiBwQja@sDvo@oFfpAoKrv@_IbBSby@cLb~@sNbbDwo@~sAwVvQ_D~lB{^fiB{^bBg@vQ_Db[oFrXcGnAz@bBRvBR~f@wBzORvQoAjC?rDRfERbGnAvGbBfEvBrDbBrIrDrDnAbBR~CRfE?fEg@fEoA~CwBrDkCfE{EfEsDzEkHjCgEjCcGjC_IfEgOnKsb@ve@_mBzYsoA~HkzEsSfEsNnK_]rIcVzJkWrDsInFcLfJsSbGcLjMcVfYce@bLoPnPgTrvE{pFvLgOzJgOfJsNvGcLrNgYjHcQbG{OrDcLfEkMzEwQv`@kgBbVkiAfuAwpGrX_oAnU{fAfh@saCjCcLzTseAr]{_BrNkp@fr@g`DnU{fAvBkMbBwLnAkMz@kMz@cLf@sN?oK?_NSkM?gES{Eg@gESgESkCg@kH{@_IoA_IwBsNwBoKcBkHkCgJcGsSoZ_`AsI_XsS_q@kH{TcGgTgE{OgJod@kCgOcBwLwBgOwBsSoAkR{@{JoAoPoKkgBg@sNoAwQ{@cQcBwVkHgfAgEkp@oA_N{Esg@wBgTwB{OwBsNgE_XsIce@wGw[cG{Yc[g_BsD_Sos@kmD_DkR_D{ToAkMcBoP{@gO{@{Tg@oP?kR?kMRcLf@sSz@sNnAwQvBgYjC_XvLsyAnK{pAvG_v@z@gOjCk\f@sNf@wLRsN?wL?cVg@_Ng@gTg@sI{@gJoAgOkCsScLgw@o_@sfCsDsXkCwVoAwQoAkWS_IS{T?cLRgOf@kWnAcQnAoPnA_NbB{O~CgYvGwj@fO_oAf^g{Cf^_zCb[{lCfm@seFzJ{w@nFw`@zE{YbGc[zJwe@vG_XfJ_]fE{OzEsNbL_]~u@oyBffAg{Cb[wy@jRod@rIkRbLcVjp@wrAby@kbBfw@s~Azw@g_BnPs]vL_XvGgOnF_NzEcLvB{EjCkHzEkMvG{JrXgh@~k@gkAnA_Df@wBf@_D?sDg@_D{@_DoKcQcG{JcBwBkCwBsXgO_Dz@wo@_]wkBccAwB{@oi@sX_]cQwLwGsD{EkH{JgEwG_DwG_DoKcBoFg@{Eg@wLSgJf@{TnAkf@SwLSwGoAoZgEsl@cB{^S_SkCw~@g@wL{@{^f@gORkMnAgJjk@fOkk@gOoAfJwBvLoAbLSjH?nKz@jRvGnAjRjCv`@vB~RbBz^fErl@nAnZRvGg@vLoAjRcB~W?~CSvGRzJR~Hf@zERbB~CzOfEvLjHvLnFvGrIfEvLvG~\bQni@rXvBz@vkBbcAvo@~z@~CvG~CvGrDvBbBz@z@nAbBz@jCf@bBRvB?vBSvBcBrIoPzc@{JrX_DzJsDzOsSf|@wBrN{ErNsNr]oKvV_Xbj@{w@z_B{w@z_B_{@rcBwo@jsA{JfT_InP{Tzh@w[jz@_eArzC_v@fxBcLr]cGjRsIv[oFjR_D~MsDnPgE~RsIrb@cG~a@{Eb[gJ~u@sl@~dFw[flCg^f{Cg^n|C{OvrAcGjf@_DjWcBrNcBvQ{@~M{@bQg@fOSvQSfYf@zTf@zOf@jMnAnPjC~WfTvwAjMz|@fJfm@zE~\bB~RnAfOz@vQRnFf@rSR~?vLSbLg@~Mg@vGg@nK{@~McBfTcGns@cLfpAwLzzAgEve@kCja@{@fOg@rNSrS?rSRjMRfJz@~Rf@vLz@rNnAfJz@zJvBjMnA~H~CnPrq@vlDfE~Rv[~}AvG~jHr]bG~\vGja@fEnZ~CzT~CzYnA~MbBjRvLrmB~Czh@bGfaAzEj_AnAbQf@~Hf@vGbBvQnAjMnAvLvGbe@fErS~CzOzErSnFrS~HbVvVzw@nF~Rb`@vmAjCnKjCvLjCbLbBvLbBvLbBzORfER~CR~CRzERvL?~M?rNg@nKSnK{@nK{@~H{@fJoAbLwBbLcVvhAsq@~~C_Nvo@g^n`B{TjdAkCbL_g@z}BoUzfA{YjsAguAnoGsb@jqBoUbcAwGbVwGrScGzOkHnPkMjWsIfO_IbLkM~R_IfJsvEfpF{OfTwLbQsXzc@_NbVgJbQcLvV_IjRgJnUkHfT{E~MkHvVgTv~@{h@nyBsDrD{JfT_IrN_I~MsDvGkHrI{EfEwGrDoFjCsDnA_NrD{Ez@kHzEg@?s]jHwGRkMR{ER{Eg@sD{@{EcBsDwBwBwBwBkCkCsDsDsIkCgEgE_DgEoAgEoAoFSgE?{h@bGsXbG_XfJkWjHgJnAoKbB{TnAcLf@wGnAoUz@cQbB{OjHkHzEoi@z^sg@zYgOvGwQjH_SnFc[bGcQjCgE?kRf@sSScQsDcQkHwLoFsI{EgJsDsIgEgJwGwGoFwQ{O_NkMoqAolA_NsN{T{Twt@we@{Y{Jc[_NoFsD{^cQ_b@cGon@cGg^_Dsb@wBcj@Ss]nAod@rDce@rIoUzEsq@zJka@vLce@bLgr@fYwe@n_@oUjRcVvVkR~WoUrXgObVsIbQkH~R{c@zw@sb@~u@_S~{JnPkH~MsIvLcL~M{ObQoPnPwVrSgYvQ_]zOoUvLsNnFcVnFwe@nKsXfEsSvB_SbBw[f@c`@?gY{@kRoAoU_DoU_D{OgE_X_IoZ{JoU_Ic[oPkW{O_SsNgYcVsb@o_@{w@os@oU_Son@_l@_l@gh@{h@we@seAgaAc~@wy@{pA_jAkWcVoZcVwVoPkRcLoKcG{J{Es]gO_b@_NwVwGwQgE{OsD{TgE_b@gEsb@wBwQg@sSSod@f@c`@bB{c@fEcV~CkRjCwQfEsSvGwQzE_SvGkRjHoP~HgTnKgOrIwL~HcL~HgOzJ_NbLgJbLcLvLgJfJsS~WkHfJw|AraC_q@~dAwe@ju@sb@zr@wQr]sNfY{JnUcLzY{J~W_N~a@{Jv`@sIr]cGr]kHjf@sDfTsNbcA_I~k@_Izc@_Irb@kHj_Iv[_Iv[sNfh@gOnd@oZrv@cLjWgJzT{O~cQr]cQ~kRzYoUn_@w[be@s]~f@_]v`@{^fc@w`@rg@{h@jp@s]v`@sb@zh@kf@~k@gc@fh@cVzY{Y~cQjRcLbL_NrNkMnKk\fY{TjR{^rXw[bVsl@b`@sb@bVod@vVogArg@wrAjf@wj@jRs]~H{TnFgJjCgTfEoPrDka@jH_SrDsb@bGgh@vG{c@nFw[jCwo@~CoUf@sXf@{h@z@oZ?{|@R{dBoAwgD{@{c@Sk_AoA_l@g@_qE{@cmAcBoeBoAwzBg@_SS_tASo`B?wj@nAcmAvGcB?cGf@cVf@_b@~Cs`AbGc`@~CsjAR_{@sDod@oFgdBwQcLkCcL_D{JsDkMoFcG_D{E_DgTwLw[_S{aAsl@gEwB_`A_l@wy@_g@c[kR{fAkp@wrAox@we@gYcwA{|@cGcG_NwLwe@sb@oP_SgJkM{EwG_NgTwGcLsD{J{JcQs]_q@gTce@_N{Y{E{J{@wBwG{OcVgh@{JgTgh@{|@cVw`@oAoAoKoP_NoU{JkMwGsIcBkCcBgEsg@{r@cGoKoA_DSgE?kC?wBnA{EjHkRjM_SzOw[jCkHvB_InAgJnAkMvB{|@RgEnAw[f@{OnAos@f@sSRwGvGcy@f@cLz@wLf@wLSc~@bBsb@f@c[z@oUSoPnAc[z@k\nAoUnF{_BfEcwAbGsmBbBs`ASolA{@olAoAo_@sDc~@gJ{pASkC{@{O{E_g@oU{sBoKox@oP_tAgEc`@_NwhA_I_q@cB{OkH{aA{@sNgEsjA?sX{@_v@Scy@oAs`Ag@kdASgc@{@ogA{@wcA_DwbDS{OSgE{@ce@g@wy@Swy@g@w`@{@{w@?sX{@kz@cBkbBSo_@SwQSkRcBon@g@oPcBkz@{EwcAcBsq@kM{nBsNsyAkM_yAsDw`@SoFkCw[kCcQg@{Jg@cL?sI?cV?{Ef@kM?oAbBwj@jCg^z@sIz@kM?{@z@sNrI{r@jHgc@bBgJvL{h@nPwj@vGgTjHgTz@wB~Mk\nFkMfE{JjCoFzJkRbGcLnKkRnZ{c@n_@sl@vL_SbQ_XbQkWz^cj@jHoKzh@cy@rb@wo@vVo_@bj@cy@vkBcsC~f@os@nx@sjAbt@_jAjWs]nAwBjgh@zm@w~@rXc`@nZ{c@nn@{aAfm@w~@vj@gw@zYsb@nPwVfOgTrq@wcAnU_]rDwGni@ku@fOwVnZ_b@ni@kz@nZsb@vL_SnK{TnKwVfJwV~Mgc@vG_XbL{m@~CoZnF{r@z@gJnAsSnFs`ArSkjCf@wGjCgErDgOfJobA~k@kMzc@_SvLsDnUoKfToKjH{Tja@vLvcAfYjM~CnKzEzJnKvQ~zJvLjWsDvt@oKbt@kMvBzErDbQrD~Rf@jCbBvLzEb`@bVsDrX{@rSvBzObGnPjMfEvG~HvQvB~RRbLgEjkCjH_NvQkWfO?fJ?rD?z@?fERjHSkH?gE?{@?sD?gJjWgO~MwQjCkHfEkScLwB_S_IwQgEwGoPkM{OcGsSwBsXz@cVrD{Ec`@cBwLg@kCsD_SsDcQwB{Ect@jMwt@nK{EjH_DjRbQ~a@bG~MjHzJbBbB~\v[~CjCfEjHvGbLnF~WnAzO?jCf@zuAvVnx@nAfEnPz^rInFfJvBvQgErSoAjHf@zTf@jMz@bLf@bBRzERjCR~Hf@fJRjH?nK?bLS~R{@rScBfEg@~Hf@jR_Dz@SnA?z@Rf@f@f@f@RbBz@bGz@jHSbB?jCg@rD{@fEoA~CoA~CoAbBwBvB_DbBkCz@wBf@kC?wB?wBg@{EcB_DwBkC_DoAgE{@{E?{ER_DRwBvGcj@rN_eAjH_v@~Hk`C~Cc~@rDcmAnAoi@f@wVSoPjCsq@fEsjAz@k\f@w[nA{qCjC_gEf@k\f@{Oz@_Nf@_NbBgTnAwQjCgT~Ck\nUgiBrS{iB~C{c@nAsSz@cVRkMf@kp@Rs~A?gYRw{I?w[R_xDRoZnAgh@f@gOf@oPvBg^vBk\fE_b@vBkRfO{kA~CwVv[wdCbLc~@rIgr@vGon@vLcrAbGcy@jC_g@jCwe@bBce@bBod@vBccAnAk_ARwe@Sce@S_jAg@w[sI_sD{@we@{@kW_Dsg@kCw[gEw[wB{OwBgOgEsSsDwQ{Jka@sSwo@cLoZ_Xsl@oUsb@_Sw[_SsX_I{JcQsScVwVw[c[gc@ko_@_XkWgOc`@_Soi@gT{c@{O{TkHkMoF{^_Sgh@oZgY_SoUwQcVwVgT{Tka@{h@wLkR{OkcQc`@kMo_@oKc`@sIw`@sDsXoA{O_Dw`@_Dku@g@{^z@{h@jCod@fE{^zEc[bL{h@zOoi@~Rsg@jRgh@vkBogFfJkWn_@gfAzOkp@rDoU~Hwe@rI{|@?Sz@_XvBku@_IocHoA{h@kHkcDsDo{AoA_v@cBogAcBcy@SkMcBobAcQksKg@oZg@kWoKkiFSgJwB{kAwGct@gEwVcGoUoKoUsDsIsDwGgJgOwQgT{TcV_X{Oco@s]koUwQ{OgJwLoKkRoKsXwV{r@kWs{@gT{^_Xkp@_Xwj@_IoKkCkCgEsDgEcBoFcB{EcGkCoF{@_DwBoF{@cGf@_XnAcL~Cs{@bBkRjCco@rDsb@rDce@f@kHz@{JvBsSb[kmDfE_l@~Hk_AvBoUzEs{@bB{m@f@kWf@cV?we@oAw~@g@oU{Esq@kCce@{E_g@{O_oAcBoKcBgOcGka@od@sdDsXsrBwVcfBkRwrAkCkRcVcfBcV{dBcBoKgTw|A_Iwj@wBsScBcQoA{Og@oKoA_b@{@o_@f@{OR_NnAs]vBg^rDkzEkjCkMnF_X~HgY~CoKvLs]rNk\vL_XbQoZrS{YbBwBni@{r@z^we@f^od@vy@{fAnZka@jf@co@bVc[zJ_N~H_NvLkR~HsNfOg^vGoPzEwQ~H{YzEcVjHgh@jCc[vBgc@RsN?sDS{Y{@c[_D_b@wBcQoAcLgEsXsN_l@cQ{r@_Ngh@cQ{r@kHo_@sDsSoAsNwBwVg@gOoA{c@RwVf@_SjCsl@fEsl@bBc[f@{T?{TS_XkCw[wBgYsD_]_D_XwBgOkCcQsDkR_DgOwGkW{JoZ{JsXoi@wmAkf@chAgYkp@c[gr@{Y_q@c`@s{@sb@gaA_b@k_A_q@o{A{EwLcG{OwQgh@kHkW_Io_@wGkwBcQ{Eg^sD_]wBcVg@sIoAoPg@{JoA_]?cVSgTf@gYz@cVrD{r@jC_]rDcj@jCw`@bBw[z@c[f@wQ?o_@g@sXg@cQkC{c@kCc[{@wGoAsIkHgc@oF_]oKgm@wLco@oKco@wL_q@cLco@wQwcAsDsSgEsSkMct@{Jsl@gJ{h@oFoZwBsSwBcQkCce@{@c`@?{^bB_v@bB{TbBgOfE{^~Hgc@vGw[bG{TvBsIbGkRbLgYvGgOrDsI~CwGnKkR~Rc[bVc[rNoPrN{OrI{JfJgJfEgEb[{TnUwQrS_NjR{Jnd@cVjwQffAoi@nd@{TnPsInPsIbVsNfc@gT~_Awe@~cQnZ{ObVgOnZsSzJsIrIwGbLgJbQcQvV_X~a@kf@fr@{w@~Ww[zY{Yv[g^z^gc@r]o_@ni@co@bQ_SfJcLjMsN~WoZv~@seArg@co@fTwVrDgEzaAsjAnPwQfaAkiAjp@ku@jdAsjAv`@{c@~HgJzEcGz^{c@jf@wj@jka@~RgYbQ{YvQw`@~Rce@rDwL~CoKf@oAfEgOfEoP~Hsb@nFo_@~C_X~C_l@f@kWf@ox@RgTRc|A?_|Bz@_]f@g^nAsSz@cQjCkWzEod@jHce@nFoZbGkWbB_IbVrInA?wBgOg@gE?kH?sDf@gEbB_DrSoP~k@gh@zE{EnFoAv[wB"
    );

    map.current.on("move", () => {
      setLng(map.current.getCenter().lng.toFixed(4));
      setLat(map.current.getCenter().lat.toFixed(4));
      setZoom(map.current.getZoom().toFixed(2));
    });

    // const marker1 = new mapboxgl.Marker()
    //   .setLngLat([18.451086189825862, -66.08392952499491])
    //   .addTo(map);
  };

  useEffect(() => {
    let isMounted = true;

    setListRouteOrders([]);
    setListRouteOrders(listData[0]);

    addRouteOrdersDeliveries(listData[0].reverse());

    // initRouteMap();

    return () => (isMounted = false);
  }, []);

  const sortList = (e) => {
    e.preventDefault();
    let routeOrdersSorted = [];

    routeOrdersSorted = listRouteOrders.sort((a, b) => {
      return new Date(b.pickup_date) - new Date(a.pickup_date);
    });

    setListRouteOrders(routeOrdersSorted);
  };

  // const onConfirmRoute = async () => {
  //   setShowConfirmPickupDate(true);
  // };

  // const confirmPendingRoute = async () => {
  //   const confirmPendingRouteResponse = await confirmPendingRouteFetch(
  //     selectedRoute
  //   );
  // };

  // const onConfirmPickupDate = async () => {
  //   setShowConfirmAlert(false);
  //   setLoading(true);
  //   const tempRoute = [selectedRoute];

  //   tempRoute.map((pickup) => {
  //     return (pickup.pickup_date = confirmPickupDateField);
  //   });

  //   setSelectedRoute(tempRoute);
  //   await confirmPendingRoute();
  //   setShowConfirmPickupDate(false);
  //   setShowSuccessAlert(true);
  //   setLoading(false);
  //   alertRouteSuccess();
  // };

  // const alertRouteSuccess = () => {
  //   setTimeout(() => {
  //     hideAlert();
  //     window.location.reload();
  //   }, 3000);
  // };

  const createMap = (index) => {
    if (!mapContainer.current) return; // initialize map only once

    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/mapbox/streets-v11",
      center: [lng, lat],
      zoom: zoom
    });

    // map.current.on("move", () => {
    //   setLng(map.current.getCenter().lng.toFixed(4));
    //   setLat(map.current.getCenter().lat.toFixed(4));
    //   setZoom(map.current.getZoom().toFixed(2));
    // });
  };

  return (
    <>
      <div className="m-3">
        <h4>{title}</h4>
      </div>
      <div className={`list-container ${className}`} style={listStyle}>
        <div className="list">
          {listHeaders.map((theader, headerIndex) => {
            return (
              <div
                className="list--list_item list-header fw-bold"
                key={`list-header-${headerIndex + 1}`}
                id={`list-header-${headerIndex + 1}`}
              >
                {theader}
                {/* {headerIndex == 1 ? (
                  <a href="#" onClick={sortList}>
                    {theader}
                  </a>
                ) : (
                  theader
                )} */}
              </div>
            );
          })}
        </div>

        <div>
          {listRouteOrders.map((pickup, rowIndex) => {
            const randomDropdownID = Math.round(Math.random() * 100) + 1;
            const pickupStatusAndColor = changePickupStatusAndColor(pickup);

            const filenamePickupDate = moment(
              new Date(pickup.pickup_date)
            ).format("MMM_DD_YYYY");

            if (!pickup.paper_signature_image) {
              pickup.paper_signature_image = null;
            }

            if (!pickup.house_image) {
              pickup.house_image = null;
            }

            const pickupSignatureImage =
              pickup.paper_signature_image !== null &&
              pickup.paper_signature_image !== undefined
                ? pickup.paper_signature_image.includes("[")
                  ? JSON.parse(pickup.paper_signature_image)[0]
                  : pickup.paper_signature_image
                : "#";

            const pickuphHouseImage =
              pickup.house_image !== null && pickup.house_image !== undefined
                ? pickup.house_image.includes("[")
                  ? JSON.parse(pickup.house_image)[0]
                  : pickup.house_image
                : "#";

            const CSVExportColumns = [
              "Order #",
              "Pickup Date",
              "Arrived Time",
              "Customer Name",
              "Business Name",
              "Route/Batch #",
              "Service",
              "Barcode",
              "Phone",
              "Email",
              "Address",
              "City",
              "State",
              "Country",
              "Latitude",
              "Longitude",
              "Comment",
              "Packages Quantity",
              "Status",
              "Tracking Link",
              "POD House Image",
              "POD Signature Image"
            ];

            const CSVExportRows = [
              listRouteOrders[rowIndex].order_number,
              listRouteOrders[rowIndex].pickup_date,
              listRouteOrders[rowIndex].arrived_time,
              listRouteOrders[rowIndex].customer_name,
              listRouteOrders[rowIndex].business_name,
              listRouteOrders[rowIndex].batch_number,
              listRouteOrders[rowIndex].service,
              listRouteOrders[rowIndex].barcode,
              listRouteOrders[rowIndex].phone,
              listRouteOrders[rowIndex].email,
              listRouteOrders[rowIndex].address,
              listRouteOrders[rowIndex].city,
              listRouteOrders[rowIndex].state,
              listRouteOrders[rowIndex].country_name,
              listRouteOrders[rowIndex].latitude,
              listRouteOrders[rowIndex].longitude,
              listRouteOrders[rowIndex].note,
              listRouteOrders[rowIndex].package_quantity,
              listRouteOrders[rowIndex].status_name,
              listRouteOrders[rowIndex].tracking_link,
              listRouteOrders[rowIndex].house_image !== null &&
              listRouteOrders[rowIndex].house_image !== undefined
                ? listRouteOrders[rowIndex].house_image.includes("[")
                  ? JSON.parse(listRouteOrders[rowIndex].house_image)[0]
                  : listRouteOrders[rowIndex].house_image
                : "#",
              listRouteOrders[rowIndex].paper_signature_image !== null &&
              listRouteOrders[rowIndex].paper_signature_image !== undefined
                ? listRouteOrders[rowIndex].paper_signature_image.includes("[")
                  ? JSON.parse(
                      listRouteOrders[rowIndex].paper_signature_image
                    )[0]
                  : listRouteOrders[rowIndex].paper_signature_image
                : "#"
            ];

            if (title !== "Pending Routes") {
              CSVExportColumns.push("Failed Reason");
              CSVExportRows.push(listRouteOrders[rowIndex].failed_reason || "");
            }

            return (
              <div key={`list-item-${rowIndex}`}>
                <div
                  style={{
                    padding: " 0 0 0 25px"
                  }}
                >
                  <CSVLink
                    style={{
                      color: "#21cace",
                      textAlign: "center"
                    }}
                    data={[
                      CSVExportColumns,
                      CSVExportRows,
                      ...listRouteOrders[rowIndex].deliveries.map(
                        (delivery, deliveryIndex) => {
                          if (
                            deliveryIndex ===
                            listRouteOrders[rowIndex].deliveries.length - 1
                          ) {
                            return [];
                          }

                          const CSVExportData = [
                            delivery.order_number,
                            delivery.pickup_date,
                            delivery.arrived_time,
                            delivery.customer_name,
                            delivery.business_name,
                            delivery.batch_number,
                            delivery.service,
                            delivery.barcode,
                            delivery.phone,
                            delivery.customer_email,
                            delivery.address,
                            delivery.city,
                            delivery.state,
                            delivery.country_name,
                            delivery.latitude,
                            delivery.longitude,
                            delivery.note,
                            delivery.package_quantity,
                            delivery.status_name,
                            delivery.tracking_link,
                            delivery.paper_signature_image !== null &&
                            delivery.paper_signature_image !== undefined
                              ? delivery.paper_signature_image.includes("[")
                                ? JSON.parse(delivery.paper_signature_image)[0]
                                : delivery.paper_signature_image
                              : "#",
                            delivery.house_image !== null &&
                            delivery.house_image !== undefined
                              ? delivery.house_image.includes("[")
                                ? JSON.parse(delivery.house_image)[0]
                                : delivery.house_image
                              : "#"
                          ];

                          if (title !== "Pending Routes") {
                            CSVExportData.push(delivery.failed_reason);
                          }

                          return CSVExportData;
                        }
                      )
                    ]}
                    filename={`route_${filenamePickupDate}.csv`}
                    target="_blank"
                    className="m-1 fw-bold"
                  >
                    Export Route
                  </CSVLink>
                </div>
                <div
                  id={`list-item-${rowIndex}`}
                  // onClick={() => setOpenCollapse(!openCollapse)}
                  className="list"
                  style={{ cursor: "pointer" }}
                  data-bs-target={`#list-item-collapse-${randomDropdownID}`}
                  data-bs-toggle="collapse"
                >
                  <div className="list--list_item mt-1 mb-1">
                    {pickup.price_estimate_id != null ? (
                      <a
                        href="#"
                        onClick={() => {
                          showEstimate(pickup.route_id);
                          getPickupTimes(pickup.route_id);
                        }}
                        className="d-block mb-2"
                      >
                        Route Estimate
                      </a>
                    ) : (
                      <div className="mb-2">No Estimate</div>
                    )}
                    {pickup.tracking_link ? (
                      <a
                        href={pickup.tracking_link}
                        className="d-block mb-2"
                        onClick={(e) => {
                          e.preventDefault();
                          navigateToTracking(pickup.tracking_link);
                        }}
                        target="_blank"
                      >
                        Track Order
                      </a>
                    ) : (
                      ""
                    )}
                    {confirmRoute && (
                      <a
                        href="#"
                        className="d-block mb-2"
                        onClick={(e) => {
                          setShowConfirmationAlert(true);
                          setSelectedRoute(listRouteOrders[rowIndex]);
                        }}
                        target="_blank"
                      >
                        Confirm Route
                      </a>
                    )}
                    {/* <a
                      href="#"
                      className="d-block mb-2"
                      onClick={() => {
                        createMap(rowIndex + 1);
                      }}
                    >
                      View Route Path
                    </a> */}
                    <Badge
                      pill
                      bg={pickupStatusAndColor.color || "secondary"}
                      className="mb-3"
                    >
                      {pickupStatusAndColor.status_name}
                    </Badge>
                  </div>
                  <div className="list--list_item">{pickup.order_number}</div>
                  <div className="list--list_item">
                    {pickup.pickup_date ||
                    pickup.pickup_date != undefined ||
                    pickup.pickup_date != null
                      ? moment(new Date(pickup.pickup_date)).format(
                          "dddd MMM DD YYYY hh:mma"
                        )
                      : "-"}
                  </div>
                  <div className="list--list_item">
                    {pickup.arrived_time
                      ? moment(new Date(pickup.arrived_time)).format("LLLL")
                      : "-"}
                  </div>
                  <div className="list--list_item">
                    {pickup.business_name || "-"}
                  </div>
                  <div className="list--list_item">
                    {pickup.batch_number || "No route number"}
                  </div>
                  <div className="list--list_item">
                    {pickup.service || "No Service"}
                  </div>
                  <div className="list--list_item">{pickup.barcode || "-"}</div>
                  <div className="list--list_item">
                    {phoneMask(pickup.phone) || "-"}
                  </div>
                  <div className="list--list_item">
                    {pickup.customer_email || "-"}
                  </div>
                  <div className="list--list_item">{pickup.address || "-"}</div>
                  <div className="list--list_item">
                    {pickup.city_name || "-"}
                  </div>
                  <div className="list--list_item">{"USA"}</div>
                  <div className="list--list_item">
                    {pickup.country_name || "-"}
                  </div>
                  <div className="list--list_item">
                    {pickup.latitude || "-"}
                  </div>
                  <div className="list--list_item">
                    {pickup.longitude || "-"}
                  </div>
                  <div className="list--list_item">{pickup.note || "-"}</div>
                  <div className="list--list_item">
                    {pickup.package_quantity || "-"}
                  </div>
                  <div className="list--list_item">
                    {pickup.failed_reason || "-"}
                  </div>
                  <div className="list--list_item d-flex pod-container">
                    {pickup.house_image ? (
                      <a
                        href={pickuphHouseImage}
                        target="_blank"
                        className="d-block"
                      >
                        <Image
                          src={pickuphHouseImage}
                          className="pod-image-badge"
                        />
                      </a>
                    ) : (
                      "-"
                    )}
                    {pickup.paper_signature_image ? (
                      <a
                        href={pickupSignatureImage}
                        target="_blank"
                        className="d-block"
                      >
                        <Image
                          src={pickupSignatureImage}
                          className="pod-image-badge"
                        />
                      </a>
                    ) : (
                      "-"
                    )}
                  </div>
                </div>
                <div
                  id={`list-item-collapse-${randomDropdownID}`}
                  className="collapse list-collapse card card-body mt-1 bg-light"
                >
                  {pickup.deliveries.map((delivery, rowIndex) => {
                    const deliveryBadgeColor = changeStatusColor(
                      delivery.status_name
                    );
                    const deliveryRandomDropdownID =
                      Math.round(Math.random() * 100) + 1;

                    if (pickup.batch_number != delivery.batch_number) {
                      return;
                    }

                    if (pickup.barcode == delivery.barcode) {
                      return;
                    }

                    if (delivery.stop_type == "pickup") {
                      return;
                    }

                    if (!delivery.paper_signature_image) {
                      delivery.paper_signature_image = null;
                    }

                    if (!delivery.house_image) {
                      delivery.house_image = null;
                    }

                    const signatureImage =
                      delivery.paper_signature_image !== null &&
                      delivery.paper_signature_image !== undefined
                        ? delivery.paper_signature_image.includes("[")
                          ? JSON.parse(delivery.paper_signature_image)[0]
                          : delivery.paper_signature_image
                        : "#";

                    const houseImage =
                      delivery.house_image !== null &&
                      delivery.house_image !== undefined
                        ? delivery.house_image.includes("[")
                          ? JSON.parse(delivery.house_image)[0]
                          : delivery.house_image
                        : "#";

                    return (
                      <>
                        <div
                          className="list"
                          key={`list-item-collapse-${rowIndex + 1}`}
                          // onClick={() => setOpenCollapse(!openCollapse)}
                        >
                          <div className="list--list_item">
                            {delivery.tracking_link ? (
                              <a
                                href={delivery.tracking_link}
                                className="d-block mb-2"
                                onClick={(e) => {
                                  e.preventDefault();
                                  navigateToTracking(delivery.tracking_link);
                                }}
                                target="_blank"
                              >
                                Track Order
                              </a>
                            ) : (
                              ""
                            )}
                            <Badge
                              pill
                              bg={deliveryBadgeColor}
                              className="mb-3"
                            >
                              {delivery.status_name}
                            </Badge>
                          </div>
                          <div className="list--list_item">
                            {delivery.order_number}
                          </div>
                          <div className="list--list_item">
                            {pickup.pickup_date ||
                            pickup.pickup_date != undefined ||
                            pickup.pickup_date != null
                              ? moment(new Date(pickup.pickup_date)).format(
                                  "dddd MMM DD YYYY hh:mma"
                                )
                              : "-"}
                          </div>
                          <div className="list--list_item">
                            {delivery.arrived_time
                              ? moment(new Date(delivery.arrived_time)).format(
                                  "LLLL"
                                )
                              : "-"}
                          </div>
                          <div className="list--list_item">
                            {delivery.dropoff_customer_name || "-"}
                          </div>
                          <div className="list--list_item">
                            {delivery.batch_number || "-"}
                          </div>
                          <div className="list--list_item">
                            {delivery.service || "-"}
                          </div>
                          <div className="list--list_item">
                            {delivery.barcode || "-"}
                          </div>
                          <div className="list--list_item">
                            {phoneMask(delivery.phone) || "-"}
                          </div>
                          <div className="list--list_item">
                            {delivery.customer_email || "-"}
                          </div>
                          <div className="list--list_item">
                            {delivery.address || "-"}
                          </div>
                          <div className="list--list_item">
                            {delivery.city_name || "-"}
                          </div>
                          <div className="list--list_item">{"USA"}</div>
                          <div className="list--list_item">
                            {delivery.country_name || "-"}
                          </div>
                          <div className="list--list_item">
                            {delivery.latitude || "-"}
                          </div>
                          <div className="list--list_item">
                            {delivery.longitude || "-"}
                          </div>
                          <div className="list--list_item">
                            {delivery.note || "-"}
                          </div>
                          <div className="list--list_item">
                            {delivery.package_quantity || "-"}
                          </div>
                          <div className="list--list_item">
                            {delivery.failed_reason || "-"}
                          </div>
                          <div className="d-flex pod-container list--list_item">
                            {delivery.house_image ? (
                              <a
                                href={houseImage}
                                target="_blank"
                                className="d-block"
                              >
                                <Image
                                  src={houseImage}
                                  className="pod-image-badge"
                                />
                              </a>
                            ) : (
                              "-"
                            )}
                            {delivery.paper_signature_image ? (
                              <a
                                href={signatureImage}
                                target="_blank"
                                className="d-block"
                              >
                                <Image
                                  src={signatureImage}
                                  className="pod-image-badge"
                                />
                              </a>
                            ) : (
                              "-"
                            )}
                          </div>
                        </div>
                      </>
                    );
                  })}
                  {/* <div className="">
                    Longitude: {lng} | Latitude: {lat}
                  </div>
                  <div
                    id="map-container"
                    ref={mapContainer}
                    className="map-container"
                  ></div> */}
                </div>
              </div>
            );
          })}
          {/* <div className="">
            Longitude: {lng} | Latitude: {lat}
          </div>
          <div
            id="map-container"
            ref={mapContainer}
            className="map-container"
          ></div> */}
        </div>
        {!!alertInfo.length && (
          <EstimateAlert
            alertInfo={alertInfo[0]}
            showEstimateAlert={showAlert}
            hideAlert={hideAlert}
            pickupTimes={pickupTimes}
            showCancelBtn={false}
            showConfirmBtn={true}
            confirmBtnText="OK"
            onEstimateAlertConfirm={hideAlert}
          />
        )}
        <ConfirmationAlert
          data={selectedRoute}
          show={showConfirmationAlert}
          setShowConfirmAlert={setShowConfirmationAlert}
        />
      </div>
    </>
  );
};

export default memo(RouteOrderList);
