import React, { useState } from "react";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { getToken } from "../../helpers/auth-helpers";
import { useSelector } from "react-redux";

import Drawer from "../Drawer";

const UserMenuButton = () => {
  const [open, setOpen] = useState(false);
  const { userAccount } = useSelector((state) => state);

  return (
    <>
      {getToken() && (
        <div
          style={{ position: "relative" }}
          className="user-menu-button-container"
        >
          <a
            href="#"
            className="d-block user-menu-button"
            // onMouseOut={() => setOpen(false)}
            // onMouseOver={() => setOpen(true)}
          >
            <div className="d-inline-block m-1">
              <FontAwesomeIcon icon={faUser} size="1x" />
            </div>
            <div className="d-inline-block">Hi, {userAccount.firstName}</div>
          </a>
          <Drawer open={open} hideDrawer={() => setOpen(false)} />
        </div>
      )}
    </>
  );
};

export default UserMenuButton;
