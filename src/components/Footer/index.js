import { Col, Container, Row } from "react-bootstrap";

const Footer = ({ style = {}, footerClass = "" }) => {
  return (
    <>
      <div
        style={{ ...footerStyle, ...style }}
        className={`footer ${footerClass}`}
      >
        <hr />
        <Container>
          <Row>
            <Col md="3"></Col>
            <Col md="6">
              <div className="footer-link-container">
                <a
                  href="https://www.goflexio.com/clientterms"
                  target="_blank"
                  className="footer-link"
                >
                  Terms of Service
                </a>
              </div>
              <div className="footer-link-container">
                <a
                  href="https://www.goflexio.com/privacy.html"
                  target="_blank"
                  className="footer-link"
                >
                  Privacy Policy
                </a>
              </div>
            </Col>
            <Col md="3"></Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

const footerStyle = {
  width: "100%",
  textAlign: "center",
  marginTop: "50px"
};

export default Footer;
