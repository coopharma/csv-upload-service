import React from "react";
import { Spinner } from "react-bootstrap";

const Loader = ({ variant = "dark", className, size = "" }) => {
  return (
    <Spinner
      animation="border"
      variant={variant}
      className={className}
      size={size}
    />
  );
};

export default Loader;
