import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Form, Button, Spinner } from "react-bootstrap";

import { setToken, setUserInfo } from "../../helpers/auth-helpers";
import { login } from "../../fetch/Auth";

// Redux
import { useDispatch } from "react-redux";
import { bindActionCreators } from "redux";
import { actionCreators } from "../../state";

const Login = () => {
  const [formData, setFormData] = useState({ email: "", password: "" });
  const [showError, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const dispatch = useDispatch();

  const { addUserInfo } = bindActionCreators(actionCreators, dispatch);

  return (
    <>
      <div className="p-1 text-center mt-2">
        <h2>Login</h2>
      </div>
      <Form onSubmit={(e) => handleSubmit(e)}>
        <Form.Group>
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="email"
            name="email"
            id="email"
            placeholder="example@gmail.com"
            value={formData.email}
            onChange={handleChange}
            required
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            name="password"
            id="password"
            placeholder="Password"
            value={formData.password}
            onChange={handleChange}
            required
          />
        </Form.Group>
        <p className="text-danger font-weight-bold">
          {showError ? showError : ""}
        </p>
        <div className="d-flex align-items-center">
          <Button
            color="primary"
            type="submit"
            className="mt-2"
            disabled={loading}
          >
            Login
          </Button>
          {loading && (
            <Spinner
              size="md"
              animation="border"
              style={{ marginLeft: "5px" }}
            />
          )}
        </div>
      </Form>
      {/* <p>email: test@gmail.com</p>
        <p>password: 12345678</p> */}
    </>
  );

  async function handleSubmit(e) {
    e.preventDefault();

    setLoading(true);

    const loginResult = await login(formData.email, formData.password);

    if (!loginResult || !loginResult.token) {
      setLoading(false);
      return setError("Email or password incorrect");
    } else {
      setLoading(false);
      setToken(loginResult.token);
      setUserInfo(JSON.stringify(loginResult));
      addUserInfo();
      return navigate("/");
    }
  }

  function handleChange(e) {
    const { name, value } = e.target;

    setFormData({
      ...formData,
      [name]: value
    });
  }
};

export default Login;
