import React from "react";
import SweetAlert from "react-bootstrap-sweetalert";

const SuccessAlert = ({
  showConfirm,
  hideAlert,
  showSuccessAlert,
  message,
  title,
  style
}) => {
  return (
    <SweetAlert
      showConfirm={showConfirm}
      success
      title={title}
      onConfirm={hideAlert}
      show={showSuccessAlert}
      style={style}
    >
      {message}
    </SweetAlert>
  );
};

export default SuccessAlert;
