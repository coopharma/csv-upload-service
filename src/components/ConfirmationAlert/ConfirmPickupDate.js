import React from "react";
import SweetAlert from "react-bootstrap-sweetalert";
import { Col, Container, Form, Row } from "react-bootstrap";
import Loader from "../Loader";

const ConfirmPickupDate = ({
  showAlert,
  onConfirmPickupDate,
  hideAlert,
  confirmPickupDateField,
  setConfirmPickupDateField,
  loading
}) => {
  return (
    <SweetAlert
      info
      title="Confirm pickup date?"
      onConfirm={onConfirmPickupDate}
      show={showAlert}
      showCancel
      confirmBtnText="Confirm Route"
      cancelBtnText="No"
      onCancel={hideAlert}
      style={{ maxWidth: "940px", width: "100%", position: "relative" }}
    >
      <Container>
        <Row className="justify-content-center">
          <Col md="6">
            <Form.Group className="m-2">
              <Form.Label>
                Pickup Date
                <small className="text-danger"> *</small>
              </Form.Label>
              <Form.Control
                type="datetime-local"
                name="confirmPickupDateField"
                value={confirmPickupDateField}
                onChange={(e) => {
                  setConfirmPickupDateField(e.target.value);
                }}
                required
              />
            </Form.Group>
          </Col>
        </Row>
      </Container>
      {loading && (
        <>
          <p>
            <br /> Please wait a few seconds while we confirm your route...{" "}
            <Loader />
          </p>
        </>
      )}
    </SweetAlert>
  );
};

export default ConfirmPickupDate;
