import React, { useState } from "react";
import SweetAlert from "react-bootstrap-sweetalert";

const ConfirmAlert = ({ hideAlert, onConfirmRoute, showConfirmAlert }) => {
  return (
    <SweetAlert
      info
      showCancel
      confirmBtnText="Yes"
      cancelBtnText="No"
      title="Confirm Route"
      onCancel={hideAlert}
      onConfirm={onConfirmRoute}
      show={showConfirmAlert}
      style={{
        maxWidth: "940px",
        width: "100%",
        position: "relative"
      }}
    >
      <h2>Are you sure you want to confirm this route?</h2>
    </SweetAlert>
  );
};

export default ConfirmAlert;
