import React, { useState } from "react";
import moment from "moment";
import ConfirmAlert from "./ConfirmAlert";
import ConfirmPickupDate from "./ConfirmPickupDate";
import SuccessAlert from "./SuccessAlert";
import { confirmPendingRouteFetch } from "../../fetch/Orders";

const ConfirmationAlert = ({ data, show, setShowConfirmAlert, pickupDate }) => {
  const [loading, setLoading] = useState(false);
  const [showSuccessAlert, setShowSuccessAlert] = useState(false);
  const [showConfirmPickupDate, setShowConfirmPickupDate] = useState(false);
  const [confirmPickupDateField, setConfirmPickupDateField] = useState(
    moment().add(1, "hour").format("yyyy-MM-DDTHH:00")
  );

  const hideAlert = () => {
    setShowConfirmAlert(false);
    setShowSuccessAlert(false);
    setShowConfirmPickupDate(false);

    setLoading(false);
    setConfirmPickupDateField(
      moment().add(1, "hour").format("yyyy-MM-DDTHH:00")
    );
  };

  const onConfirmRoute = async () => {
    setShowConfirmPickupDate(true);
  };

  const confirmPendingRoute = async (pendingData) => {
    const confirmPendingRouteResponse = await confirmPendingRouteFetch(
      pendingData
    );

    console.log(confirmPendingRouteResponse);

    return confirmPendingRouteResponse;
  };

  const onConfirmPickupDate = async () => {
    setShowConfirmAlert(false);
    setLoading(true);
    const tempRoute = [data];

    tempRoute.map((pickup) => {
      return (pickup.pickup_date = confirmPickupDateField);
    });

    await confirmPendingRoute(data);
    setShowConfirmPickupDate(false);
    setShowSuccessAlert(true);
    setLoading(false);
    alertRouteSuccess();
  };

  const alertRouteSuccess = () => {
    setTimeout(() => {
      window.location.reload();
    }, 3000);
  };
  return (
    <>
      <ConfirmAlert
        hideAlert={hideAlert}
        onConfirmRoute={onConfirmRoute}
        showConfirmAlert={show}
      />

      <ConfirmPickupDate
        showAlert={showConfirmPickupDate}
        onConfirmPickupDate={onConfirmPickupDate}
        hideAlert={hideAlert}
        confirmPickupDateField={confirmPickupDateField}
        setConfirmPickupDateField={setConfirmPickupDateField}
        loading={loading}
      />
      <SuccessAlert
        showConfirm={false}
        hideAlert={hideAlert}
        showSuccessAlert={showSuccessAlert}
        message="Page will refresh..."
        title="Route confirmation is successful"
        style={{ maxWidth: "940px", width: "100%", position: "relative" }}
      />
    </>
  );
};

export default ConfirmationAlert;
