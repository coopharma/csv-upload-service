import React from "react";
import { Spinner, Image } from "react-bootstrap";
import logoImg from "../../images/Flexio_color.png";

const LogoLoader = () => {
  return (
    <div style={logoLoaderStyle}>
      <Image src={logoImg} width="300" />
      <Spinner animation="border" variant="dark" />
    </div>
  );
};

const logoLoaderStyle = {
  position: "fixed",
  width: "100%",
  height: "100%",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center"
};

export default LogoLoader;
