import React from "react";
import { Form } from "react-bootstrap";
import HelpTooltip from "../Tootip/HelpTooltip";

const FileUpload = ({ onFileChange, fileRef }) => {
  return (
    <Form.Group>
      <Form.Label>
        Upload CSV file
        <small className="text-danger"> *</small>
        <HelpTooltip
          tooltipText="Please fill out at least the required columns before uploading. This
        CSV upload optimizes you route and shows the estimate total time and
        distance, and the estimate prices."
        />
      </Form.Label>
      <Form.Control
        type="file"
        size="lg"
        name="fileUpload"
        onChange={onFileChange}
        ref={fileRef}
        accept=".csv, .txt"
        required
      ></Form.Control>
    </Form.Group>
  );
};

export default FileUpload;
