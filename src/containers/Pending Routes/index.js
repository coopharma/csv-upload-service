import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import RouteOrderList from "../../components/Lists/RouteOrderList";
import {
  pendingRoutesOrders,
  allPendingRoutesOrders
} from "../../fetch/Orders";
import Loader from "../../components/Loader";

const PendingRoutes = () => {
  const [pendingRoutes, setPendingRoutes] = useState([]);
  const [loading, setLoading] = useState(true);
  const [message, setMessage] = useState("");
  const { isAdmin } = useSelector((state) => state.userAccount);

  useEffect(async () => {
    let pendingRoutesData;

    setLoading(true);
    if (isAdmin) {
      pendingRoutesData = await allPendingRoutesOrders();
    } else {
      pendingRoutesData = await pendingRoutesOrders();
    }

    if (!pendingRoutesData.data.length) {
      setMessage(pendingRoutesData.message);
    }

    setLoading(false);
    setPendingRoutes(pendingRoutesData.data);
    return () => false;
  }, []);
  return (
    <div>
      {!!pendingRoutes.length && (
        <div className="table-responsive">
          <RouteOrderList
            confirmRoute={true}
            title="Pending Routes"
            listData={[pendingRoutes]}
            listHeaders={[
              "",
              "Order #",
              "Pickup Date",
              "Arrived Time",
              "Business Name",
              "Route/Batch #",
              "Service",
              "Barcode",
              "Phone",
              "Email",
              "Address",
              "City",
              "State",
              "Country",
              "Latitude",
              "Longitude",
              "Comment",
              "Packages Quantity",
              "POD"
            ]}
            className=""
          />
        </div>
      )}
      {loading && (
        <div className="text-center">
          <Loader />
        </div>
      )}
      <h3 className="text-center mt-4">{message}</h3>
    </div>
  );
};

export default PendingRoutes;
