import React, { useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import Register from "../../components/Register";

const AddUser = () => {
  // const [newUserData, setNewUserData] = useState({
  //     firstName: "",
  //     lastName: "",
  //     businessName: "",
  //     firstName: "",
  // })
  // const handleChange = (e) => {
  //     setNewUserData({
  //         ...newUserData
  //     })
  // }
  return (
    <Container>
      <Row>
        <Col md="8" style={{ margin: "auto" }}>
          <p>
            <Link to="/user-list">{"<"} Back</Link>
          </p>
          <Register title="Add User" />
        </Col>
      </Row>
    </Container>
    // <Form>
    //   <Form.Group>
    //       <Form.Label>First Name</Form.Label>
    //       <Form.Control
    //       type="text"
    //       placeholder="First Name"
    //       name="firstName"
    //       value={this.state.newUserData.firstName}
    //       onChange={(e) => this.handleChange(e)}/>
    //   </Form.Group>
    // </Form>
  );
};

export default AddUser;
