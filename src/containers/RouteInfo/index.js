import { useState, useEffect } from "react";
import { useParams, useNavigate, Link } from "react-router-dom";
import { useSelector } from "react-redux";
import SimpleList from "../../components/Lists/SimpleList";
import { Container, Card } from "react-bootstrap";

const RouteInfo = () => {
  const { routeOrders } = useSelector((state) => state);
  const { routeNumber } = useParams();
  const navigate = useNavigate();

  const [deliveries, setDeliveries] = useState([]);

  useEffect(() => {
    let newDeliveries = [...new Set(routeOrders)];
    newDeliveries = newDeliveries.filter(
      (delivery) => delivery.batch_number == routeNumber
    );

    console.log(newDeliveries);

    setDeliveries(newDeliveries);

    return () => false;
  }, []);

  return (
    <Container>
      <Link to="/route-orders" className="d-block mb-2">
        Back
      </Link>
      <h2>#{routeNumber}</h2>

      <SimpleList
        listData={deliveries}
        listHeaders={[
          "",
          "Business Name",
          "Route/Batch #",
          "Service",
          "Barcode",
          "Phone",
          "Email",
          "Address",
          "City",
          "State",
          "Country",
          "Latitude",
          "Longitude",
          "Comment",
          "Packages Quantity",
          "Pickup Date",
          "Tracking Link",
          "Status"
        ]}
      />
    </Container>
  );
};

export default RouteInfo;
