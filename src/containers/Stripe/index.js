import React, { useEffect } from "react";

// Stripe
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import SetupForm from "./SetupForm";

// Make sure to call `loadStripe` outside of a component’s render to avoid
// recreating the `Stripe` object on every render.
const stripePromise = loadStripe(process.env.REACT_APP_STRIPE_PUBLISHABLE_KEY);

const PaymentElement = ({
  clientSecret,
  onIntentCreated = () => "Stripe success",
  buttonText
}) => {
  const options = {
    // passing the client secret obtained in step 2
    clientSecret,
    // Fully customizable with appearance API.
    appearance: {
      /*...*/
    }
  };

  return (
    <Elements stripe={stripePromise} options={options}>
      <SetupForm onSubmitForm={onIntentCreated} buttonText={buttonText} />
    </Elements>
  );
};

export default PaymentElement;
