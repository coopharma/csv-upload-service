import React, { useState, useEffect, memo, useRef } from "react";
import RouteOrderList from "../../components/Lists/RouteOrderList";
import {
  getAllRouteOrders,
  getOrdersByRouteNumber,
  getRouteNumbers
} from "../../fetch/Orders";
import { Container, Spinner, Form, Row, Col, Button } from "react-bootstrap";
import moment from "moment";
import { fetchIsAdmin } from "../../fetch/User";
import { useSelector } from "react-redux";

const ActiveRoutes = () => {
  const [orders, setOrders] = useState([]);
  const [loading, setLoading] = useState(null);
  const [routeNumbers, setRouteNumbers] = useState([]);
  const [selectedOrder, setSelectedOrder] = useState([]);
  const [initialDate, setInitialDate] = useState(
    moment().format("yyyy-MM-DDT07:00")
  );
  const [endDate, setEndDate] = useState(moment().format("yyyy-MM-DDT17:00"));
  const [error, setError] = useState("");
  const [message, setMessage] = useState("");

  const { userID, isAdmin } = useSelector((state) => state.userAccount);

  const orderComponent = useRef(true);

  useEffect(async () => {
    let isMounted = true;
    setLoading(true);

    if (orderComponent.current) {
      setLoading(false);
    }

    return () => (isMounted = false);
  }, []);

  const handleChange = async (e) => {
    const { value } = e.target;

    if (!value.length) {
      setSelectedOrder([]);
      setLoading(false);
      return;
    }
  };

  const submitDateRange = async (e) => {
    e.preventDefault();
    setLoading(true);

    setSelectedOrder([]);
    let orderSelected = [];

    if (!initialDate.length && !endDate.length) {
      return;
    }

    let initialDatetime = moment(new Date(initialDate));
    let endDatetime = moment(new Date(endDate));

    if (endDatetime.isBefore(initialDatetime)) {
      setSelectedOrder([]);
      setError("End date must be in the future.");
      setMessage("");
      setLoading(false);
      return;
    } else {
      setError("");
      setMessage("");
    }

    let routesResponse;

    initialDatetime = initialDatetime.format("MM-DD-YYYY hh:mm");
    endDatetime = endDatetime.format("MM-DD-YYYY hh:mm");

    if (isAdmin) {
      routesResponse = await getAllRouteOrders(initialDatetime, endDatetime);
    } else {
      routesResponse = await getOrdersByRouteNumber(
        initialDatetime,
        endDatetime
      );
    }

    if (!routesResponse.userOrders.length) {
      setMessage("No orders found.");
      setSelectedOrder([]);
      setLoading(false);
      return;
    }

    setTimeout(() => {
      setSelectedOrder(routesResponse.userOrders);
      setLoading(false);
    }, 1000);
  };

  return (
    <>
      <Container ref={orderComponent}>
        <h2 className="text-center mb-3">Active Routes</h2>
        <Row>
          <Col md="12" className="m-auto">
            <Form onSubmit={submitDateRange}>
              <Row className="justify-content-center">
                <Col md="3">
                  <Form.Group className="d-flex">
                    {/* <Form.Select disabled={loading} onChange={(e) => handleChange(e)}>
                <option value="">Choose a route number</option>;
                {routeNumbers.map((routeNumber, routeIndex) => {
                  return (
                    <option key={routeIndex} value={routeNumber}>
                      {routeNumber}
                    </option>
                  );
                })}
              </Form.Select> */}
                    <Form.Control
                      type="datetime-local"
                      name="initalDate"
                      value={initialDate}
                      onChange={(e) => setInitialDate(e.target.value)}
                      required
                    />
                  </Form.Group>
                </Col>
                <Col md="3">
                  <Form.Group>
                    <Form.Control
                      type="datetime-local"
                      name="endDate"
                      value={endDate}
                      onChange={(e) => setEndDate(e.target.value)}
                      required
                    />
                  </Form.Group>
                </Col>
                <Col md="1">
                  <Button type="submit" disabled={loading}>
                    Search
                  </Button>
                </Col>
              </Row>
              {loading && (
                <div className="d-flex justify-content-center">
                  <Spinner
                    size="md"
                    animation="border"
                    className="text-center mt-2"
                    style={{ marginLeft: "5px" }}
                  />
                </div>
              )}
              {error && <p className="text-danger m-1">{error}</p>}
              {message && <p className="m-3 text-center">{message}</p>}
            </Form>
          </Col>
        </Row>
      </Container>
      {!!selectedOrder.length && (
        <div className="table-responsive">
          <RouteOrderList
            title="Routes"
            listData={[selectedOrder]}
            listHeaders={[
              "",
              "Order #",
              "Pickup Date",
              "Arrived Time",
              "Business Name",
              "Route/Batch #",
              "Service",
              "Barcode",
              "Phone",
              "Email",
              "Address",
              "City",
              "State",
              "Country",
              "Latitude",
              "Longitude",
              "Comment",
              "Packages Quantity",
              "Failed Reason",
              "POD"
            ]}
            className=""
          />
        </div>
      )}
      <div>
        {!orders.length ? (
          <h4 className="text-center mt-3">{orders.message}</h4>
        ) : (
          ""
        )}
      </div>
    </>
  );
};

export default memo(ActiveRoutes);
