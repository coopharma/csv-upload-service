import React from "react";
import { Container, Row, Col } from "react-bootstrap";

import CSVForm from "../CSVForm";

const Home = ({ isAdmin }) => {
  return (
    <Container>
      <Row>
        <Col md="7" className="m-auto">
          <CSVForm isAdmin={isAdmin} />
        </Col>
      </Row>
    </Container>
  );
};

export default Home;
