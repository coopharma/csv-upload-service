import React, { useEffect, useState, useRef } from "react";
import { Container, Tabs, Tab, Form, Row, Col, Button } from "react-bootstrap";
import Loader from "../../components/Loader";
import { getFees, updateFuelSurchargeFee } from "../../fetch/Fees";
import { isNotaNumber } from "../../helpers/util";

import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import { actionCreators } from "../../state";

const RouteSettings = () => {
  const routeSettingsElem = useRef(false);
  const [tabKey, setTabKey] = useState("fees");
  const [fees, setFees] = useState({
    fuelSurcharge: 0
  });
  const [editMode, setEditMode] = useState(false);
  const [loading, setLoading] = useState(false);
  const [metadata, setMetadata] = useState({ type: "", message: "" });

  const dispatch = useDispatch();
  const { addFees } = bindActionCreators(actionCreators, dispatch);
  const { fees: stateFees } = useSelector((state) => state);

  useEffect(async () => {
    if (routeSettingsElem.current) {
      setFees({
        ...fees,
        fuelSurcharge: stateFees.fuel_surcharge.toFixed(2)
      });
    }

    return () => (routeSettingsElem.current = true);
  }, []);

  const handleFeesChange = (e) => {
    setFees({ ...fees, [e.target.name]: e.target.value });
  };

  const onSubmitForm = async (e) => {
    e.preventDefault();

    if (!fees.fuelSurcharge) {
      setMetadata({
        ...metadata,
        type: "error",
        message: "The field cannot be empty."
      });
      return;
    }

    if (!isNotaNumber(parseFloat(fees.fuelSurcharge))) {
      setMetadata({
        ...metadata,
        type: "error",
        message: "Please enter a real number."
      });
      return;
    }

    setLoading(true);

    const fuelSurchargeData = await updateFuelSurchargeFee(fees.fuelSurcharge);

    setLoading(false);

    if (fuelSurchargeData.error != undefined) {
      setFees({
        ...fees
      });
      setMetadata({
        ...metadata,
        type: "error",
        message: fuelSurchargeData.error
      });

      return;
    }

    setFees({
      ...fees,
      fuelSurcharge: fuelSurchargeData.fuelSurcharge
    });
    addFees({ fuel_surcharge: fuelSurchargeData.fuelSurcharge });
    setEditMode(false);
    setMetadata({
      ...metadata,
      type: "success",
      message: fuelSurchargeData.message
    });
  };

  return (
    <div ref={routeSettingsElem}>
      <Container>
        <h3>Route Settings</h3>

        <Form onSubmit={onSubmitForm}>
          <Tabs activeKey={tabKey} onSelect={(k) => setTabKey(k)}>
            <Tab eventKey="fees" title="Fees">
              <Row className="pt-4">
                <Col md={4}>
                  <Form.Group className="m-2">
                    <Form.Label>Fuel Surcharge </Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Fuel Surcharge"
                      name="fuelSurcharge"
                      value={fees.fuelSurcharge}
                      onChange={(e) => handleFeesChange(e)}
                      max={1}
                      min={0}
                      disabled={!editMode}
                    />
                  </Form.Group>
                </Col>
              </Row>
            </Tab>
            {/* <Tab eventKey="house" title="House" className="text-danger">
            lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem
            ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum
          </Tab> */}
          </Tabs>
          <Button
            hidden={editMode}
            onClick={() => setEditMode(true)}
            className="m-2"
          >
            Edit
          </Button>
          <Button type="submit" hidden={!editMode} className="m-2">
            Save
          </Button>
          <Button
            hidden={!editMode}
            onClick={() => {
              setFees({ ...fees, fuelSurcharge: stateFees.fuel_surcharge });
              setEditMode(false);
              setMetadata({ ...metadata, type: "", message: "" });
            }}
            className="m-2"
            variant="outline-primary"
          >
            Cancel
          </Button>
          {loading && <Loader />}
          {metadata.message && (
            <p
              className={`ms-2 ${
                metadata.type === "error" ? "text-danger" : "text-success"
              } fw-bold`}
            >
              {metadata.message}
            </p>
          )}
        </Form>
      </Container>
    </div>
  );
};

export default RouteSettings;
