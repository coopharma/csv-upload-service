import React, { useEffect, useState } from "react";
import ErrorList from "../../components/Lists/ErrorList";

const Error = () => {
  const [allErrors, setAllErrors] = useState({ message: "No data", data: [] });
  useEffect(async () => {
    const allErrors = await fetch(
      `${process.env.REACT_APP_API_URL}/api/error/getAllErrors`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }
    );

    const errorData = await allErrors.json();

    setAllErrors(errorData);

    return () => false;
  }, []);
  return (
    <div>
      {!allErrors.data.length && (
        <h5 className="text-center">{allErrors.message}</h5>
      )}
      {!!allErrors.data.length && (
        <ErrorList
          errorData={allErrors.data}
          headerData={["Name", "Description", "Date"]}
        />
      )}
    </div>
  );
};

export default Error;
