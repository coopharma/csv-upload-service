import React, { useEffect } from "react";
import { getUserInfo } from "../../helpers/auth-helpers";
import { useNavigate } from "react-router-dom";

const PrivateRoute = ({ Component }) => {
  let navigate = useNavigate();
  const userInfo = JSON.parse(getUserInfo());

  useEffect(() => {
    if (!userInfo.isAdmin) {
      navigate("/", { replace: true });
    }
  });

  return <Component />;
};

export default PrivateRoute;
