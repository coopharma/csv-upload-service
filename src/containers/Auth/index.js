import React, { useEffect } from "react";
import { Container, Row, Col, Tab, Tabs, Card } from "react-bootstrap";
import Login from "../../components/Login";
import Register from "../../components/Register";

const Auth = () => {
  useEffect(() => {
    init();
  }, []);

  const init = () => {
    window.initHippo({
      appSecretKey: "4e1a82dad7a343b1825806803dd17a02",
      language: "en",
      tags: ["Client", "Not Registered"],
      botGroupID: 2644
    });
  };
  return (
    <div style={{ maxWidth: "700px", margin: "auto" }}>
      <Row className="justify-content-center">
        <Col md="8">
          <Tabs defaultActiveKey="login" transition>
            <Tab eventKey="login" title="Login">
              <Login />
            </Tab>
            <Tab eventKey="register" title="Register">
              <Register title="Register" />
            </Tab>
          </Tabs>
        </Col>
      </Row>
    </div>
  );
};

export default Auth;
