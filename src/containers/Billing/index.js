import React, { useState, useEffect } from "react";
import {
  Table,
  Container,
  Form,
  Row,
  Col,
  ToastContainer,
  Toast
} from "react-bootstrap";
import moment from "moment";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle } from "@fortawesome/free-solid-svg-icons";
import {
  getClientSecret,
  getPaymentMethods,
  removeCustomerPaymentMethod,
  updateCustomerPaymentMethod
} from "../../fetch/Stripe";

import Loader from "../../components/Loader";

import { useSelector } from "react-redux";
import SweetAlert from "react-bootstrap-sweetalert";

import { Link } from "react-router-dom";

import StripeUI from "../Stripe";

const Billing = () => {
  const { userAccount } = useSelector((state) => state);
  const [loading, setLoading] = useState(false);
  const [editLoading, setEditLoading] = useState(false);
  const [paymentMethodsData, setPaymentMethodsData] = useState({
    creditCard: [],
    bankAccount: []
  });
  const [stripeData, setStripeData] = useState([
    {
      customerName: "Cristian McGuiver",
      cardNumber: "4242424242424242",
      bankAccount: "345768809"
    }
  ]);
  const [showUpdatePaymentMethodAlert, setShowUpdatePaymentMethodAlert] =
    useState(false);
  const [editPaymentMethodData, setEditPaymentMethodData] = useState({
    creditCard: {
      expMonth: "",
      expYear: "",
      address: "",
      fullName: "",
      city: "",
      postalCode: "",
      phone: ""
    }
  });
  const [showSuccess, setShowSuccess] = useState(false);
  const [showErrorMessage, setShowErrorMessage] = useState(false);
  const [paymentMethodType, setPaymentMethodType] = useState("");
  const [message, setMessage] = useState("");
  const [stripeClientSecret, setStripeClientSecret] = useState("");
  const [showStripeAlert, setShowStripeAlert] = useState(false);
  const [stripeAlertTitle, setStripeAlertTitle] = useState("");

  const amountOfDots = (dotsLength) => {
    return Array.from(
      { length: dotsLength },
      (v, i) => "*"
      // <FontAwesomeIcon icon={faCircle} style={{ margin: "1px" }} key={i} />
    ).map((item) => item);
  };

  useEffect(async () => {
    await fetchCustomerPaymentMethods();

    return () => true;
  }, []);

  const fetchCustomerPaymentMethods = async () => {
    setLoading(true);
    const customerPaymentMethods = await getPaymentMethods(
      userAccount.customer_id
    );

    customerPaymentMethods.paymentMethods.map((paymentMethod) => {
      const creditCard = Object.keys(paymentMethod).find(
        (item) => item === "card"
      );

      if (creditCard !== undefined) {
        setPaymentMethodsData({
          creditCard: [
            {
              ...paymentMethod.billing_details,
              ...paymentMethod.card
            }
          ],
          bankAccount: []
        });
      } else {
        setPaymentMethodsData({
          bankAccount: [
            {
              ...paymentMethod.billing_details,
              ...paymentMethod.us_bank_account
            }
          ],
          creditCard: []
        });
      }
    });

    setLoading(false);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;

    setEditPaymentMethodData({
      creditCard: {
        ...editPaymentMethodData.creditCard,
        [name]: value
      }
    });
  };

  const onEditConfirm = async () => {
    setEditLoading(true);

    const updateEditPaymentMethodData = {
      paymentMethodType: paymentMethodType,
      ...editPaymentMethodData
    };
    const updatePMResponse = await updateCustomerPaymentMethod(
      userAccount.customer_id,
      updateEditPaymentMethodData
    );

    if (updatePMResponse.error) {
      setShowSuccess(false);
      setMessage(updatePMResponse.error);
      setShowErrorMessage(true);
    } else {
      setMessage(updatePMResponse.message);
      setShowErrorMessage(false);
      setShowSuccess(true);
    }

    setEditLoading(false);
    setShowUpdatePaymentMethodAlert(false);
    setEditPaymentMethodData({
      creditCard: {
        expMonth: "",
        expYear: "",
        address: "",
        fullName: "",
        city: "",
        postalCode: "",
        phone: ""
      }
    });

    await fetchCustomerPaymentMethods();
  };

  const removePaymentMethod = async (customerID, pmType) => {
    setLoading(true);

    const removeCustomerPaymentMethodResponse =
      await removeCustomerPaymentMethod(customerID, pmType);

    setLoading(false);

    if (removeCustomerPaymentMethodResponse.error) {
      setMessage(removeCustomerPaymentMethodResponse.error);
      setShowErrorMessage(true);
    }
  };

  return (
    <Container>
      <ToastContainer className="p-3" position="top-center">
        <Toast
          onClose={() => setShowSuccess(false)}
          show={showSuccess}
          delay={5000}
          autohide
          style={{
            backgroundColor: "#21cace",
            width: "640px",
            color: "#fff"
          }}
        >
          {/* <Toast.Header closeButton={false}>
              <img
                src="holder.js/20x20?text=%20"
                className="rounded me-2"
                alt=""
              />
              <strong className="me-auto">Bootstrap</strong>
              <small>11 mins ago</small>
            </Toast.Header> */}
          <Toast.Body className="text-center">
            <div
              className="font-weight-bold"
              style={{ fontSize: "1.5em", fontWeight: "bold" }}
            >
              {message}
            </div>
          </Toast.Body>
        </Toast>
      </ToastContainer>
      <ToastContainer className="p-3" position="top-center">
        <Toast
          onClose={() => setShowErrorMessage(false)}
          show={showErrorMessage}
          delay={5000}
          autohide
          style={{
            backgroundColor: "#dc3545",
            width: "640px",
            color: "#fff"
          }}
        >
          {/* <Toast.Header closeButton={false}>
              <img
                src="holder.js/20x20?text=%20"
                className="rounded me-2"
                alt=""
              />
              <strong className="me-auto">Bootstrap</strong>
              <small>11 mins ago</small>
            </Toast.Header> */}
          <Toast.Body className="text-center">
            <div
              className="font-weight-bold"
              style={{ fontSize: "1.5em", fontWeight: "bold" }}
            >
              {message}
            </div>
          </Toast.Body>
        </Toast>
      </ToastContainer>
      <h1>Billing</h1>
      <p>Here you can manage your payment information.</p>

      <h3 className="mt-4 mb-4">Payment Methods</h3>
      {loading && <Loader size="sm" />}
      <h5 className="mt-3">Credit Cards</h5>
      {!!paymentMethodsData.creditCard.length ? (
        <>
          <Table>
            <thead>
              <tr>
                <td>Name</td>
                <td>Expiry Date</td>
                <td>Card Number</td>
                <td>Actions</td>
              </tr>
            </thead>
            <tbody>
              {paymentMethodsData.creditCard.map((stripeInfo, index) => (
                <tr key={index}>
                  <td>{stripeInfo.name}</td>
                  <td>
                    {`${moment()
                      .month(stripeInfo.exp_month - 1)
                      .format("MMMM")}, ${stripeInfo.exp_year}`}
                  </td>
                  <td>
                    {amountOfDots(12)}
                    {stripeInfo.last4}
                  </td>
                  <td>
                    <div className="list-menu">
                      <a
                        href="#"
                        onClick={() => {
                          setPaymentMethodType("card");
                          setEditPaymentMethodData({
                            creditCard: {
                              expMonth:
                                paymentMethodsData.creditCard[0].exp_month,
                              expYear:
                                paymentMethodsData.creditCard[0].exp_year,
                              address:
                                paymentMethodsData.creditCard[0].address.line1,
                              fullName: paymentMethodsData.creditCard[0].name,
                              city: paymentMethodsData.creditCard[0].address
                                .city,
                              postalCode:
                                paymentMethodsData.creditCard[0].address
                                  .postal_code,
                              phone: paymentMethodsData.creditCard[0].phone
                            }
                          });
                          setShowUpdatePaymentMethodAlert(true);
                        }}
                      >
                        Edit
                      </a>
                      <a
                        href="#"
                        onClick={async () => {
                          await removePaymentMethod(
                            userAccount.customer_id,
                            "card"
                          );
                          await fetchCustomerPaymentMethods();
                        }}
                      >
                        Remove
                      </a>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </>
      ) : (
        <p>No credit cards</p>
      )}
      <a
        href="#"
        className="m-2 cta-button"
        onClick={async () => {
          setStripeAlertTitle("Add Card");
          const { client_secret } = await getClientSecret(
            userAccount.customer_id,
            "card"
          );

          setStripeClientSecret(client_secret);
          setShowStripeAlert(true);
        }}
      >
        Add Card
      </a>
      <h5 className="mt-4">Bank Accounts</h5>
      {!!paymentMethodsData.bankAccount.length ? (
        <>
          <Table>
            <thead>
              <tr>
                <td>Name</td>
                <td>Account #</td>
                <td>Bank Name</td>
                <td>Actions</td>
              </tr>
            </thead>
            <tbody>
              {paymentMethodsData.bankAccount.map((stripeInfo, index) => (
                <tr key={index}>
                  <td>{stripeInfo.name}</td>
                  <td>
                    {amountOfDots(4)}
                    {stripeInfo.last4}
                  </td>
                  <td>{stripeInfo.bank_name}</td>
                  <td>
                    <a
                      href="#"
                      onClick={async () => {
                        await removePaymentMethod(
                          userAccount.customer_id,
                          "us_bank_account"
                        );
                        await fetchCustomerPaymentMethods();
                      }}
                    >
                      Remove
                    </a>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </>
      ) : (
        <p>No bank Accounts</p>
      )}
      <a
        href="#"
        className="m-2 cta-button"
        onClick={async () => {
          setStripeAlertTitle("Add Bank Account");
          const { client_secret } = await getClientSecret(
            userAccount.customer_id,
            "us_bank_account"
          );

          setStripeClientSecret(client_secret);
          setShowStripeAlert(true);
        }}
      >
        Add Bank Account
      </a>
      {(!!paymentMethodsData.creditCard.length ||
        !!paymentMethodsData.bankAccount.length) && (
        <SweetAlert
          showCancel
          confirmBtnText="Save"
          title="Edit Card"
          onCancel={() => {
            setEditPaymentMethodData({
              creditCard: {
                expMonth: "",
                expYear: "",
                address: "",
                fullName: "",
                city: "",
                postalCode: "",
                phone: ""
              }
            });
            setShowUpdatePaymentMethodAlert(false);
          }}
          onConfirm={() => onEditConfirm()}
          show={showUpdatePaymentMethodAlert}
          style={{
            maxWidth: "940px",
            width: "100%"
          }}
        >
          <Form className="mt-3 text-left">
            <Container>
              <Row>
                <Col md="6" className="m-auto">
                  <Row>
                    <Col md="6">
                      <Form.Group>
                        <Form.Label>Exp Month</Form.Label>
                        <Form.Select
                          name="expMonth"
                          value={editPaymentMethodData.creditCard.expMonth}
                          onChange={handleChange}
                          // defaultValue={paymentMethodsData.creditCard[0].exp_month}
                          className="mb-3"
                        >
                          <option>Select a month</option>
                          {Array.from(
                            { length: 12 },
                            (_, index) => index + 1
                          ).map((item, index) => (
                            <option key={index}>{item}</option>
                          ))}
                        </Form.Select>
                      </Form.Group>
                    </Col>
                    <Col md="6">
                      <Form.Group>
                        <Form.Label>Exp Year</Form.Label>
                        <Form.Select
                          name="expYear"
                          value={editPaymentMethodData.creditCard.expYear}
                          onChange={handleChange}
                          className="mb-3"
                        >
                          <option>Select a year</option>
                          {Array.from(
                            { length: 50 },
                            (_, index) => moment().year() + index
                          ).map((item, index) => (
                            <option key={index}>{item}</option>
                          ))}
                        </Form.Select>
                      </Form.Group>
                    </Col>
                  </Row>

                  <Form.Group>
                    <Form.Label>Full Name</Form.Label>
                    <Form.Control
                      type="text"
                      name="fullName"
                      maxLength="40"
                      value={editPaymentMethodData.creditCard.fullName}
                      onChange={handleChange}
                      className="mb-3"
                    />
                  </Form.Group>

                  <Form.Group>
                    <Form.Label>Address</Form.Label>
                    <Form.Control
                      type="text"
                      name="address"
                      maxLength="200"
                      value={editPaymentMethodData.creditCard.address}
                      onChange={handleChange}
                      className="mb-3"
                    />
                  </Form.Group>

                  <Form.Group>
                    <Form.Label>City</Form.Label>
                    <Form.Control
                      type="text"
                      name="city"
                      maxLength="40"
                      value={editPaymentMethodData.creditCard.city}
                      onChange={handleChange}
                      className="mb-3"
                    />
                  </Form.Group>

                  <Form.Group>
                    <Form.Label>Postal Code</Form.Label>
                    <Form.Control
                      type="text"
                      name="postalCode"
                      value={editPaymentMethodData.creditCard.postalCode}
                      onChange={handleChange}
                      className="mb-3"
                    />
                  </Form.Group>

                  <Form.Group>
                    <Form.Label>Phone Number</Form.Label>
                    <Form.Control
                      type="text"
                      name="phone"
                      maxLength="10"
                      value={editPaymentMethodData.creditCard.phone}
                      onChange={handleChange}
                      className="mb-3"
                    />
                  </Form.Group>
                </Col>
              </Row>
              {editLoading && <Loader />}
            </Container>
          </Form>
        </SweetAlert>
      )}
      {stripeClientSecret && (
        <SweetAlert
          title={stripeAlertTitle}
          confirmBtnText="Submit"
          cancelBtnText="Cancel"
          showCancel={false}
          showConfirm={false}
          onConfirm={() => true}
          show={showStripeAlert}
          onCancel={() => {
            setShowStripeAlert(false);
          }}
        >
          <StripeUI
            clientSecret={stripeClientSecret}
            onIntentCreated={async () => {
              setMessage("SUCCESS!!");
              await fetchCustomerPaymentMethods();
              setShowStripeAlert(false);
            }}
          />
        </SweetAlert>
      )}
    </Container>
  );
};

export default Billing;
