import React from "react";
import { Link } from "react-router-dom";
import {
  Container,
  Form,
  Button,
  Row,
  Col,
  Alert,
  Card,
  Accordion
} from "react-bootstrap";
import { fileUpload } from "../../fetch/File";
import { getBusinesses } from "../../fetch/Business/index";
import {
  attachPaymentMethodToCustomer,
  getClientSecret,
  verifyStripeCustomer
} from "../../fetch/Stripe";
import moment from "moment";
import SweetAlert from "react-bootstrap-sweetalert";

// Components
import FileUpload from "../../components/FileUpload";
import Loader from "../../components/Loader";
import { CSVLink } from "react-csv";
import EstimateAlert from "../../components/Estimate/EstimateAlert.js";
import { confirmRouteFetch } from "../../fetch/Orders";

import { connect } from "react-redux";

import StripeUI from "../../containers/Stripe";
import { addUserInfo } from "../../state/action-creators";
import { getToken, setUserInfo } from "../../helpers/auth-helpers";

// Media
// import flexioCSVTemplate from "../../documents/Flexio_CSV_Template.csv";

class OrderForm extends React.Component {
  constructor() {
    super();

    this.state = {
      pickupDate: "",
      routeNumber: "",
      customerName: "",
      driverID: "",
      // teamName: "",
      service: "",
      error: "",
      showError: false,
      success: false,
      loading: false,
      processing: false,
      files: null,
      showAlertMessage: false,
      showPendingMessage: false,
      vehicleType: "",
      zones: "",
      businessData: [],
      businessSelected: "",
      loadingBusiness: false,
      otherService: "",
      showAlert: false,
      formDataToSend: "",
      confirmLoading: false,
      cancelLoading: false,
      showAlertEstimate: false,
      estimateAlertData: [],
      routeData: [],
      showPaymentMethodAlert: false,
      showFillPaymentMethodInfoAlert: false,
      fillPaymentMethodInfo: {
        cardName: "",
        cardNumber: "",
        paymentMethod: "",
        expDate: "",
        cvc: "",
        customerID: ""
      },
      paymentMethodAlertMessage: "",
      stripeClientSecret: "",
      showStripeAlert: false,
      showPODExportAlert: false,
      podOrderNumber: "",
      PODError: false,
      PODErrorMessage: ""
    };

    this.formRef = React.createRef();
    this.fileRef = React.createRef();
  }

  componentDidMount() {
    if (this.props.isAdmin) {
      this.fetchBusiness();
    }
  }

  async fetchBusiness() {
    this.setState({
      loadingBusiness: true
    });

    const businesses = await getBusinesses();

    this.setState({
      loadingBusiness: false,
      businessData: businesses
    });
  }

  onFileChange(e) {
    this.setState({
      files: e.target.files[0]
    });
  }

  clearField() {
    // Clear form and state
    this.formRef.current.reset();
    this.setState({
      files: null,
      // customerName: "",
      // teamName: "",
      pickupDate: "",
      routeNumber: "",
      driverID: "",
      service: "",
      vehicleType: "",
      zones: "",
      businessSelected: "",
      otherService: ""
    });
  }

  notANumber(value) {
    if (isNaN(value)) {
      return false;
    }

    return true;
  }

  async postFile(e) {
    e.preventDefault();

    this.setState({
      error: false,
      showError: false,
      success: false,
      loading: true,
      processing: true
    });

    if (!this.state.files) {
      this.setState({
        error: "Please upload a document.",
        success: false,
        loading: false,
        showError: true,
        processing: false
      });

      return;
    }

    const datetime = moment(new Date(this.state.pickupDate));
    const currentDate = moment();

    if (datetime.isBefore(currentDate)) {
      this.setState({
        error: "Please choose a pickup date that is from today to the future.",
        showError: true,
        loading: false,
        processing: false
      });
      return;
    }

    if (datetime.diff(currentDate, "days") < 0) {
      this.setState({
        error: "You need to choose the current date or a future date.",
        showError: true,
        success: false,
        loading: false,
        processing: false
      });
      return;
    }

    if (!(datetime.hour() >= 6) || !(datetime.hour() <= 17)) {
      this.setState({
        error: "You need to choose a time between 6:00am and 5:00pm",
        showError: true,
        success: "",
        loading: false,
        processing: false
      });
      return;
    }

    let verifiedCustomer;
    let verifiedPaymentMethod;
    let clientSecretResponse = { client_secret: "" };

    if (!this.props.userAccount.isAdmin) {
      if (
        !this.props.userAccount.customer_id ||
        this.props.userAccount.customer_id === undefined
      ) {
        verifiedCustomer = await verifyStripeCustomer(this.props.userAccount);

        setUserInfo(
          JSON.stringify({
            ...this.props.userAccount,
            customer_id: verifiedCustomer.customer_id
          })
        );

        addUserInfo();

        clientSecretResponse = await getClientSecret(
          verifiedCustomer.customer_id,
          ""
        );
      } else {
        clientSecretResponse = await getClientSecret(
          this.props.userAccount.customer_id,
          ""
        );
      }
    }

    // Create setup intent when customer
    // does not have payment method
    if (clientSecretResponse.client_secret) {
      return this.setState({
        stripeClientSecret: clientSecretResponse.client_secret,
        showStripeAlert: true
      });
    }

    console.log("save route");
    const savedData = await this.saveRoute();
  }

  onEstimateAlertConfirm = async () => {
    this.setState({
      confirmLoading: true
    });

    const confirmedData = await this.confirmRoute(this.state.routeData);

    this.setState({
      success: confirmedData.message,
      loading: false,
      processing: false,
      confirmLoading: false,
      showAlertMessage: false,
      showError: false,
      error: "",
      showAlert: false,
      showAlertEstimate: false
    });

    // Clear form and state
    this.formRef.current.reset();
    this.clearField();
  };

  onEstimateAlertCancel = () => {
    this.setState({
      cancelLoading: true,
      showAlert: false,
      loading: false,
      showPendingMessage: true,
      processing: false,
      cancelLoading: false,
      showAlertEstimate: false
    });

    this.clearField();
  };

  // onAlertConfirm = async () => {

  // };

  onAlertCancel = async () => {
    this.setState({
      cancelLoading: true
    });

    this.setState({
      showAlert: false,
      loading: false,
      showPendingMessage: true,
      processing: false,
      cancelLoading: false
    });

    this.clearField();
  };

  confirmRoute = async (route) => {
    const confirmRouteRes = await confirmRouteFetch(route);

    return confirmRouteRes;
  };

  onPaymentMethodConfirm = async () => {
    this.setState({
      showFillPaymentMethodInfoAlert: true,
      fillPaymentMethodInfo: {
        ...this.state.fillPaymentMethodInfo,
        customerID: this.props.userAccount.customer_id
      }
    });
  };

  onFillPaymentMethodConfirm = async () => {
    try {
      const attachPaymentMethodResponse = await attachPaymentMethodToCustomer(
        this.state.fillPaymentMethodInfo
      );

      if (attachPaymentMethodResponse.status != 200) {
        return this.setState({
          paymentMethodAlertMessage: attachPaymentMethodResponse.message
        });
      }

      this.setState({
        paymentMethodAlertMessage: "Your estimate is being processed..."
      });

      const savedData = await this.saveRoute();
    } catch (error) {
      console.log(error);
    }
    return 0;
  };

  saveRoute = async () => {
    const formData = new FormData();
    const formDataJson = {
      pickupDate: this.state.pickupDate,
      routeNumber: this.state.routeNumber,
      customerName: this.state.customerName,
      driverID: this.state.driverID,
      // teamName: this.state.teamName,
      service:
        this.state.service != "Other"
          ? this.state.service
          : this.state.otherService,
      vehicleType: this.state.vehicleType,
      zones: this.state.zones,
      businessSelected: this.state.businessSelected
    };

    formData.append("fileUpload", this.state.files, this.state.files.name);
    formData.append("driverInfo", JSON.stringify(formDataJson));

    const fileUploadResponse = await fileUpload(formData);

    if (fileUploadResponse.error != undefined) {
      this.setState({
        error: fileUploadResponse.error,
        success: false,
        loading: false,
        showError: true,
        processing: false,
        confirmLoading: false,
        showAlert: false
      });
    }

    this.setState({
      routeData: fileUploadResponse.data,
      estimateAlertData: [
        {
          ...fileUploadResponse.data[0],
          package_quantity: fileUploadResponse.data[0].items.length,
          deliveries: fileUploadResponse.data.length - 1
        }
      ],
      showPaymentMethodAlert: false,
      showAlertEstimate: true,
      showFillPaymentMethodInfoAlert: false,
      showStripeAlert: false,
      showPendingMessage: false
    });

    return {};
    // return fileUploadResponse;
  };

  handleChange(e) {
    const key = e.target.name;
    const value = e.target.value;

    this.setState({
      [key]: value
    });
  }

  handlePaymentMethodChange(e) {
    const key = e.target.name;
    const value = e.target.value;

    if (
      (key === "cardNumber" && isNaN(value)) ||
      (key === "cvc" && isNaN(value))
    ) {
      return;
    }

    this.setState((prevState) => {
      let paymentMethodInfo = { ...prevState.fillPaymentMethodInfo };
      paymentMethodInfo[key] = value;

      return {
        fillPaymentMethodInfo: paymentMethodInfo
      };
    });
  }

  hideAlert() {
    this.setState({
      showAlertMessage: false,
      showAlertEstimate: false,
      showPaymentMethodAlert: false,
      showFillPaymentMethodInfoAlert: false
    });
  }

  render() {
    return (
      <>
        <Container fluid>
          {!this.state.loadingBusiness && (
            <Card>
              <Card.Body>
                <h2 className="text-center mb-3">Create Estimate</h2>
                <Form
                  className="mt-3"
                  onSubmit={(e) => this.postFile(e)}
                  ref={this.formRef}
                >
                  <FileUpload
                    onFileChange={(e) => this.onFileChange(e)}
                    fileRef={this.fileRef}
                    style={{ display: "inline-block" }}
                  />

                  <div className="d-flex justify-content-end">
                    <a
                      href="#"
                      onClick={() => this.setState({ showAlertMessage: true })}
                    >
                      <span style={{ verticalAlign: "middle" }}>
                        Download CSV Template
                      </span>
                    </a>
                  </div>

                  <div>
                    <a
                      href="#"
                      onClick={() =>
                        this.setState({
                          showPODExportAlert: !this.state.showPODExportAlert
                        })
                      }
                    >
                      Export POD PDF
                    </a>
                  </div>

                  {this.state.businessData.length != 0 && (
                    <>
                      <Row>
                        <Col md="6">
                          <Form.Group className="m-2">
                            <Form.Label>
                              Business <small className="text-danger"> *</small>
                            </Form.Label>
                            <Form.Select
                              required
                              onChange={(e) => this.handleChange(e)}
                              name="businessSelected"
                              id="businessSelected"
                              value={this.state.businessSelected}
                            >
                              <option value="">Choose a business</option>
                              {this.state.businessData.map(
                                (business, index) => {
                                  return (
                                    <option
                                      value={business.business_name}
                                      key={`${business.business_name}_${index}`}
                                    >
                                      {business.business_name}
                                    </option>
                                  );
                                }
                              )}
                            </Form.Select>
                          </Form.Group>
                        </Col>
                      </Row>
                    </>
                  )}

                  <Row>
                    {/* <Col md="4">
                  <Form.Group className="m-2">
                    <Form.Label>Business Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter customer name"
                      name="customerName"
                      value={this.state.customerName}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </Form.Group>
                </Col> */}

                    <Col md="6">
                      <Form.Group className="m-2">
                        <Form.Label>
                          Pickup Date Time
                          <small className="text-danger"> *</small>
                        </Form.Label>
                        <Form.Control
                          type="datetime-local"
                          name="pickupDate"
                          value={this.state.pickupDate}
                          onChange={(e) => this.handleChange(e)}
                          required
                        />
                      </Form.Group>
                    </Col>

                    <Col md="6">
                      <Form.Group className="m-2">
                        <Form.Label>
                          Service <small className="text-danger"> *</small>
                        </Form.Label>
                        <Form.Select
                          required
                          onChange={(e) => this.handleChange(e)}
                          name="service"
                          id="service"
                          value={this.state.service}
                        >
                          <option value="">Select an option</option>
                          <option value="Flexio Shift">Flexio Shift</option>
                          <option value="Flexio Now">Flexio Now</option>
                          <option value="Per Package">Per Package</option>
                          <option value="Truck Type 1">Truck Type 1</option>
                          <option value="Truck Type 2">Truck Type 2</option>
                          <option value="Other">Other</option>
                        </Form.Select>
                      </Form.Group>
                      <Form.Group
                        className="m-2"
                        hidden={this.state.service === "Other" ? false : true}
                      >
                        <Form.Label>Other Service</Form.Label>
                        <Form.Control
                          type="text"
                          name="otherService"
                          value={this.state.otherService}
                          required={
                            this.state.service === "Other" ? true : false
                          }
                          onChange={(e) => this.handleChange(e)}
                        />
                      </Form.Group>
                    </Col>
                  </Row>
                  {/* <h5 className="m-2">Optional:</h5> */}
                  <Accordion defaultActiveKey={0} className="m-2 mt-4">
                    <Accordion.Item eventKey="0">
                      <Accordion.Header>Optional Fields</Accordion.Header>
                      <Accordion.Body>
                        <Row>
                          <Col md="6">
                            <Form.Group className="m-2">
                              <Form.Label>Route #</Form.Label>
                              <Form.Control
                                type="text"
                                placeholder="Enter route number"
                                name="routeNumber"
                                value={this.state.routeNumber}
                                onChange={(e) => this.handleChange(e)}
                              />
                            </Form.Group>
                          </Col>

                          <Col md="6">
                            <Form.Group className="m-2">
                              <Form.Label>Driver ID </Form.Label>
                              <Form.Control
                                type="text"
                                placeholder="Enter driver ID"
                                name="driverID"
                                value={this.state.driverID}
                                onChange={(e) => this.handleChange(e)}
                              />
                            </Form.Group>
                          </Col>
                          {/* <Col md="4">
                  <Form.Group className="m-2">
                    <Form.Label>
                      Team Name <small className="text-danger"> *</small>
                    </Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter team name"
                      name="teamName"
                      value={this.state.teamName}
                      onChange={(e) => this.handleChange(e)}
                      required
                    />
                  </Form.Group>
                </Col> */}
                        </Row>
                        <Row>
                          <Col md="6">
                            <Form.Group className="m-2">
                              <Form.Label>Vehicle Type</Form.Label>
                              <Form.Select
                                onChange={(e) => this.handleChange(e)}
                                name="vehicleType"
                                id="vehicleType"
                                value={this.state.vehicleType}
                              >
                                <option value="">Select an option</option>
                                <option value="Carros">Carros</option>
                                <option value="SUV">SUV</option>
                                <option value="Vans SM">Vans SM</option>
                                <option value="Vans XL">Vans XL</option>
                              </Form.Select>
                            </Form.Group>
                          </Col>
                          <Col md="6">
                            <Form.Group className="m-2">
                              <Form.Label>Zones</Form.Label>
                              <Form.Select
                                onChange={(e) => this.handleChange(e)}
                                name="zones"
                                id="zones"
                                value={this.state.zones}
                              >
                                <option value="">Select an option</option>
                                <option value="Metro">Metro</option>
                                <option value="Norte 1">Norte 1</option>
                                <option value="Norte 2">Norte 2</option>
                                <option value="Sur 1">Sur 1</option>
                                <option value="Sur 2">Sur 2</option>
                                <option value="Este 1">Este 1</option>
                                <option value="Este 2">Este 2</option>
                                <option value="Oeste 1">Oeste 1</option>
                                <option value="Oeste 2">Oeste 2</option>
                                <option value="Centro 1">Centro 1</option>
                                <option value="Centro 2">Centro 2</option>
                              </Form.Select>
                            </Form.Group>
                          </Col>
                        </Row>
                      </Accordion.Body>
                    </Accordion.Item>
                  </Accordion>

                  <Form.Group className="mt-2 mb-2 d-inline-block m-2">
                    <Button
                      type="button"
                      variant="primary"
                      onClick={(e) => this.clearField(e)}
                    >
                      Clear
                    </Button>
                  </Form.Group>
                  <Form.Group className="d-inline-block">
                    <Button
                      type="Submit"
                      variant="primary"
                      disabled={this.state.loading}
                    >
                      {this.state.loading ? (
                        <Loader variant="light" />
                      ) : (
                        "Create Estimate"
                      )}
                    </Button>
                  </Form.Group>
                </Form>
              </Card.Body>
            </Card>
          )}
          <>
            {this.state.showError && (
              <Alert
                variant="danger"
                className="mt-3"
                onClose={() =>
                  this.setState({
                    error: false,
                    showError: false
                  })
                }
                dismissible
              >
                {this.state.error}
              </Alert>
            )}
            {this.state.success && (
              <Alert variant="success" className="mt-3">
                {this.state.success}
              </Alert>
            )}
            {this.state.processing && (
              <Alert variant="info" className="mt-3">
                Processing...
              </Alert>
            )}
            {this.state.loadingBusiness && (
              <Alert
                variant="info"
                className="mt-3 d-flex justify-content-start align-items-center"
              >
                <Loader variant="dark" />
                <div className="m-2">Loading businesses...</div>
              </Alert>
            )}
            {this.state.showPendingMessage && (
              <Alert variant="info" className="mt-3">
                You can confirm your Pending Routes{" "}
                <Link to="pending-routes">here</Link>.
              </Alert>
            )}
          </>
          <SweetAlert
            info
            title="CSV Template Steps"
            onConfirm={() => this.hideAlert()}
            show={this.state.showAlertMessage}
            // style={{ maxWidth: "720px", width: "100%" }}
          >
            <div className="text-start">
              <p>1- Fill out in row #2 the Pickup Location information</p>
              <p>
                2- Fill out in row #3 and on all the Delivery Locations
                information
              </p>
              <p>Required Columns:</p>
              <ul>
                <li>Customer Name</li>
                <li>Phone</li>
                <li>Address Field 1</li>
                <li>City</li>
                <li>State</li>
                <li>Time Window (Ex: 8:00,18:00)</li>
                <li>Service Time (in minutes)</li>
              </ul>
              <p>The other columns are optional.</p>
              <CSVLink
                data={[
                  [
                    "Order#",
                    "Barcode_Number",
                    "Customer_Name",
                    "Phone",
                    "Customer_Email",
                    "Address_Field_1",
                    "Address_Field_2",
                    "City",
                    "State",
                    "Country",
                    "Latitude",
                    "Longitude",
                    "Comment",
                    "Packages_Quantity",
                    "Time_Windows",
                    "Service_Time",
                    "Internal_User_ID",
                    "Internal_Items_ID",
                    "Copay"
                  ],
                  [
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                  ],
                  [
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                  ]
                ]}
                filename="Flexio_CSV_Template.csv"
                target="_blank"
                className="m-1 fw-bold"
              >
                Download
              </CSVLink>
            </div>
          </SweetAlert>
        </Container>

        {!!this.state.estimateAlertData.length && (
          <EstimateAlert
            alertInfo={this.state.estimateAlertData[0]}
            showEstimateAlert={this.state.showAlertEstimate}
            hideAlert={() => this.setState({ showAlertEstimate: false })}
            onEstimateAlertConfirm={this.onEstimateAlertConfirm}
            onEstimateAlertCancel={this.onEstimateAlertCancel}
            pickupTimes={{}}
            showCancelBtn={true}
            showConfirmBtn={true}
            confirmBtnText="Confirm Route"
            cancelBtnText="Save For Later"
          >
            {this.state.confirmLoading && (
              <>
                <Loader />
                <p>
                  <br /> Please wait a few seconds while we confirm your
                  route...
                </p>
              </>
            )}
            {this.state.cancelLoading && (
              <>
                <Loader />
                <p>
                  <br /> Saving for later...
                </p>
              </>
            )}
          </EstimateAlert>
        )}
        <SweetAlert
          title="You don't have a Payment Method"
          confirmBtnText="Yes"
          cancelBtnText="No"
          showCancel
          showConfirm
          onConfirm={this.onPaymentMethodConfirm}
          show={this.state.showPaymentMethodAlert}
          onCancel={() => {
            this.setState({
              showPaymentMethodAlert: false,
              success: false,
              loading: false,
              showError: false,
              processing: false,
              confirmLoading: false,
              showAlert: false
            });

            this.clearField();
          }}
        >
          <p>
            You need to have a payment method in order to create or save a
            route. Do you wish to add a payment method?
          </p>
        </SweetAlert>
        <SweetAlert
          title="Add Payment Method"
          confirmBtnText="Submit"
          cancelBtnText="Cancel"
          showCancel
          showConfirm
          onConfirm={this.onFillPaymentMethodConfirm}
          show={this.state.showFillPaymentMethodInfoAlert}
          onCancel={() => {
            this.setState({
              showFillPaymentMethodInfoAlert: false,
              success: false,
              loading: false,
              showError: false,
              processing: false,
              confirmLoading: false,
              showAlert: false
            });

            this.clearField();
            this.hideAlert();
          }}
        >
          <Form.Group className="m-2">
            <Form.Label>Payment Method</Form.Label>
            <Form.Select
              onChange={(e) => this.handlePaymentMethodChange(e)}
              name="paymentMethod"
              value={this.state.fillPaymentMethodInfo.paymentMethod}
            >
              <option value="">Select an option</option>
              <option value="us_bank_account">Bank Account</option>
              <option value="card">Credit Card</option>
            </Form.Select>
          </Form.Group>
          {/* <Form.Group className="m-2">
            <Form.Label>Card Name</Form.Label>
            <Form.Control
              type="text"
              name="cardName"
              value={this.state.fillPaymentMethodInfo.cardName}
              placeholder="Jhon Doe"
              required
              onChange={(e) => this.handlePaymentMethodChange(e)}
            />
          </Form.Group> */}
          <Form.Group className="m-2">
            <Form.Label>Card Number</Form.Label>
            <Form.Control
              type="text"
              inputMode="numeric"
              name="cardNumber"
              value={this.state.fillPaymentMethodInfo.cardNumber}
              placeholder="xxxx xxxx xxxx xxxx"
              required
              onChange={(e) => this.handlePaymentMethodChange(e)}
              maxLength="16"
              // pattern="[0-9\s]{13,19}"
            />
          </Form.Group>
          <Form.Group className="m-2">
            <Form.Label>Expiration Date</Form.Label>
            <Form.Control
              type="datetime-local"
              name="expDate"
              value={this.state.fillPaymentMethodInfo.expDate}
              onChange={(e) => this.handlePaymentMethodChange(e)}
              required
            />
          </Form.Group>
          <Form.Group className="m-2">
            <Form.Label>Card Number</Form.Label>
            <Form.Control
              type="text"
              inputMode="numeric"
              name="cvc"
              value={this.state.fillPaymentMethodInfo.cvc}
              placeholder="xxx"
              required
              onChange={(e) => this.handlePaymentMethodChange(e)}
              maxLength="3"
            />
          </Form.Group>
          <p>{this.state.paymentMethodAlertMessage}</p>
        </SweetAlert>
        {this.state.stripeClientSecret && (
          <SweetAlert
            title=""
            confirmBtnText="Submit"
            cancelBtnText="Cancel"
            showCancel={false}
            showConfirm={false}
            onConfirm={() => alert("Payment stored successfully.")}
            show={this.state.showStripeAlert}
            onCancel={() => {
              this.setState({
                showStripeAlert: false
              });
            }}
          >
            <StripeUI
              clientSecret={this.state.stripeClientSecret}
              onIntentCreated={this.saveRoute}
            />
          </SweetAlert>
        )}
        <SweetAlert
          title="Export POD PDF"
          onConfirm={async () => {
            try {
              this.setState({
                loading: true
              });
              const orderStatusRes = await fetch(
                `${process.env.REACT_APP_API_URL}/api/status/exportPOD`,
                {
                  method: "POST",
                  body: JSON.stringify({
                    orderNumber: this.state.podOrderNumber
                  }),
                  headers: {
                    "Content-Type": "application/json",
                    Authorization: getToken()
                  }
                }
              );
              const orderStatusData = await orderStatusRes.json();

              if (!Object.keys(orderStatusData.data).length) {
                this.setState({
                  loading: false,
                  PODErrorMessage: orderStatusData.message,
                  PODError: true
                });
                return;
              }

              const link = document.createElement("a");
              link.href = `data:application/pdf;base64,${orderStatusData.data.POD}`;
              link.target = "_blank";
              link.download = `${
                orderStatusData.data.orderNumber
              }_${new Date().getTime()}.pdf`;
              link.click();
              // window.open(link.href, "_blank");
            } catch (error) {
              console.log();
            }

            this.setState({
              loading: false,
              showPODExportAlert: false,
              podOrderNumber: "",
              PODError: false
            });
          }}
          onCancel={() => {
            this.setState({
              showPODExportAlert: false,
              podOrderNumber: "",
              PODError: false
            });
          }}
          cancelBtnText={"Cancel"}
          confirmBtnText={"Export"}
          showCancel={true}
          showConfirm={true}
          show={this.state.showPODExportAlert}
          style={{ maxWidth: "940px", width: "100%", position: "relative" }}
        >
          <Container>
            <Row>
              <Col md={6} className="m-auto">
                <Form.Group className="m-2">
                  <Form.Label>Enter order number:</Form.Label>
                  <Form.Control
                    type="text"
                    name="orderNumber"
                    value={this.state.podOrderNumber}
                    onChange={(e) => {
                      this.setState({
                        podOrderNumber: e.target.value
                      });
                      console.log(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>
            </Row>
            {this.state.loading && <Loader />}
            {this.state.PODError && (
              <div className="text-danger">{this.state.PODErrorMessage}</div>
            )}
          </Container>
        </SweetAlert>
        {/* <SweetAlert
          title="Confirm this route"
          confirmBtnText="Confirm Route"
          cancelBtnText="Save for later"
          showCancel
          showConfirm
          onConfirm={this.onAlertConfirm}
          show={this.state.showAlert}
          onCancel={this.onAlertCancel}
        >
          <h3>Do you want to confirm this route?</h3>
          {this.state.confirmLoading && (
            <>
              <Loader />
              <p>
                <br /> Please wait a few seconds while we confirm your route...
              </p>
            </>
          )}
          {this.state.cancelLoading && (
            <>
              <Loader />
              <p>
                <br /> Saving for later...
              </p>
            </>
          )}
        </SweetAlert> */}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userAccount: state.userAccount
  };
};

export default connect(mapStateToProps)(OrderForm);
