import { getToken } from "../../helpers/auth-helpers";

const verifyStripeCustomer = async (customerData) => {
  const customerVerified = await fetch(
    `${process.env.REACT_APP_API_URL}/api/stripe/verifyCustomer`,
    {
      method: "POST",
      body: JSON.stringify(customerData),
      headers: {
        "Content-Type": "application/json",
        Authorization: getToken()
      }
    }
  );

  const customerVerifiedData = await customerVerified.json();

  return customerVerifiedData;
};

const verifyStripePaymentMethod = async (customerID) => {
  const paymentMethodVerified = await fetch(
    `${process.env.REACT_APP_API_URL}/api/stripe/verifyPaymentMethod`,
    {
      method: "POST",
      body: JSON.stringify({ customer_id: customerID }),
      headers: {
        "Content-Type": "application/json",
        Authorization: getToken()
      }
    }
  );

  const paymentMethodVerifiedData = await paymentMethodVerified.json();

  return paymentMethodVerifiedData;
};

const attachPaymentMethodToCustomer = async (paymentMethodData) => {
  const attachPaymentMethodResponse = await fetch(
    `${process.env.REACT_APP_API_URL}/api/stripe/attachPaymentMethodToCustomer`,
    {
      method: "POST",
      body: JSON.stringify(paymentMethodData),
      headers: {
        "Content-Type": "application/json",
        Authorization: getToken()
      }
    }
  );

  const attachPaymentMethodData = await attachPaymentMethodResponse.json();

  return attachPaymentMethodData;
};

const getClientSecret = async (customerID, paymentMethodType) => {
  const getClientSecretResponse = await fetch(
    `${process.env.REACT_APP_API_URL}/api/stripe/clientSecret`,
    {
      method: "POST",
      body: JSON.stringify({ customerID, paymentMethodType }),
      headers: {
        "Content-Type": "application/json",
        Authorization: getToken()
      }
    }
  );

  return await getClientSecretResponse.json();
};

const getPaymentMethods = async (customerID) => {
  const getPaymentMethodsResponse = await fetch(
    `${process.env.REACT_APP_API_URL}/api/stripe/retrievePaymentMethods`,
    {
      method: "POST",
      body: JSON.stringify({ customerID }),
      headers: {
        "Content-Type": "application/json",
        Authorization: getToken()
      }
    }
  );

  return await getPaymentMethodsResponse.json();
};

const updateCustomerPaymentMethod = async (customerID, paymentMethodData) => {
  const updatePaymentMethodResponse = await fetch(
    `${process.env.REACT_APP_API_URL}/api/stripe/updatePaymentMethod`,
    {
      method: "POST",
      body: JSON.stringify({ ...paymentMethodData, customerID }),
      headers: {
        "Content-Type": "application/json",
        Authorization: getToken()
      }
    }
  );

  return await updatePaymentMethodResponse.json();
};

const removeCustomerPaymentMethod = async (customerID, paymentMethodType) => {
  const removePaymentMethodResponse = await fetch(
    `${process.env.REACT_APP_API_URL}/api/stripe/removePaymentMethod`,
    {
      method: "POST",
      body: JSON.stringify({ customerID, paymentMethodType }),
      headers: {
        "Content-Type": "application/json",
        Authorization: getToken()
      }
    }
  );

  return await removePaymentMethodResponse.json();
};

export {
  verifyStripeCustomer,
  verifyStripePaymentMethod,
  attachPaymentMethodToCustomer,
  getClientSecret,
  getPaymentMethods,
  updateCustomerPaymentMethod,
  removeCustomerPaymentMethod
};
