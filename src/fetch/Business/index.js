const getBusinesses = async () => {
  const getBusinessesResponse = await fetch(
    `${process.env.REACT_APP_API_URL}/api/business/getAllBusinesses`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    }
  );

  const getBusinessesData = await getBusinessesResponse.json();

  return getBusinessesData.data;
};

export { getBusinesses };
