import { getToken } from "../../helpers/auth-helpers";

const getFees = async () => {
  const feesResponse = await fetch(
    `${process.env.REACT_APP_API_URL}/api/fees/getFees`
  );

  const feesResponseData = await feesResponse.json();

  return feesResponseData;
};

const updateFuelSurchargeFee = async (fuelSurchargeFee) => {
  const updateSurchargeFeeResponse = await fetch(
    `${process.env.REACT_APP_API_URL}/api/fees/updateFuelSurchargeFee`,
    {
      method: "POST",
      body: JSON.stringify({ fuelSurchargeFee }),
      headers: {
        Authorization: getToken(),
        "Content-Type": "application/json"
      }
    }
  );

  const updateSurchargeFeeResponseData =
    await updateSurchargeFeeResponse.json();

  return updateSurchargeFeeResponseData;
};

export { updateFuelSurchargeFee, getFees };
