import { getToken } from "../../helpers/auth-helpers";

const fileUpload = async (formData) => {
  try {
    const fetchResponse = await fetch(
      `${process.env.REACT_APP_FILE_URL}/csvUploadService`,
      {
        method: "POST",
        body: formData,
        headers: {
          Authorization: getToken()
        }
      }
    );

    const fetchData = await fetchResponse.json();

    return fetchData;
  } catch (error) {
    console.log("Fetch error fileUpload()", error);
  }
};

export { fileUpload };
