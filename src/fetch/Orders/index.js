import { getToken } from "../../helpers/auth-helpers";

const getOrders = async () => {
  try {
    const fetchResponse = await fetch(
      `${process.env.REACT_APP_API_URL}/api/order/userOrders`,
      {
        method: "GET",
        headers: {
          Authorization: getToken(),
          "Content-Type": "application/json"
        }
      }
    );

    const fetchData = await fetchResponse.json();

    return fetchData;
  } catch (error) {
    console.log("Fetch error getOrders()", error);
  }
};

const getOrdersByRouteNumber = async (initialDate, endDate) => {
  try {
    const fetchResponse = await fetch(
      `${process.env.REACT_APP_API_URL}/api/order/userOrdersByRouteNumber`,
      {
        method: "POST",
        body: JSON.stringify({ initialDate, endDate }),
        headers: {
          Authorization: getToken(),
          "Content-Type": "application/json"
        }
      }
    );

    // const reader = fetchResponse.body.getReader()

    // console.log(+fetchResponse.headers.get("content-length"));

    const fetchData = await fetchResponse.json();

    return fetchData;
  } catch (error) {
    console.log("Fetch error getOrdersByRouteNumber()", error);
  }
};

const getRouteNumbers = async () => {
  try {
    const fetchResponse = await fetch(
      `${process.env.REACT_APP_API_URL}/api/order/getRouteNumbers`,
      {
        method: "GET",
        headers: {
          Authorization: getToken(),
          "Content-Type": "application/json"
        }
      }
    );

    const fetchData = await fetchResponse.json();

    return fetchData;
  } catch (error) {
    console.log("Fetch error getRouteNumbers()", error);
  }
};

const getAllRouteOrders = async (initialDate, endDate) => {
  try {
    const fetchResponse = await fetch(
      `${process.env.REACT_APP_API_URL}/api/order/getAllRouteOrders`,
      {
        method: "POST",
        body: JSON.stringify({ initialDate, endDate }),
        headers: {
          Authorization: getToken(),
          "Content-Type": "application/json"
        }
      }
    );

    const fetchData = await fetchResponse.json();

    return fetchData;
  } catch (error) {
    console.log("Fetch error getRouteNumbers()", error);
  }
};

const pendingRoutesOrders = async () => {
  try {
    const pendingRoutesResponse = await fetch(
      `${process.env.REACT_APP_API_URL}/api/order/pendingRoutes`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: getToken()
        }
      }
    );

    let pendingRoutesResponseData = await pendingRoutesResponse.json();

    return pendingRoutesResponseData;
  } catch (error) {
    console.log("Fetch error pendingRoutesOrders()", error);
  }
};

const allPendingRoutesOrders = async () => {
  try {
    const allPendingRoutesResponse = await fetch(
      `${process.env.REACT_APP_API_URL}/api/order/allPendingRoutes`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: getToken()
        }
      }
    );

    let allPendingRoutesResponseData = await allPendingRoutesResponse.json();

    return allPendingRoutesResponseData;
  } catch (error) {
    console.log("Fetch error allPendingRoutesOrders()", error);
  }
};

const confirmPendingRouteFetch = async (route) => {
  try {
    const confirmPendingRouteResponse = await fetch(
      `${process.env.REACT_APP_API_URL}/api/order/confirmPendingRoute`,
      {
        method: "POST",
        body: JSON.stringify([route]),
        headers: {
          "Content-Type": "application/json",
          Authorization: getToken()
        }
      }
    );

    let confirmPendingRouteResponseData =
      await confirmPendingRouteResponse.json();

    return confirmPendingRouteResponseData;
  } catch (error) {
    console.log("Fetch error confirmPendingRouteFetch()", error);
  }
};
const confirmRouteFetch = async (route) => {
  try {
    const confirmRouteResponse = await fetch(
      `${process.env.REACT_APP_API_URL}/api/order/confirmRoute`,
      {
        method: "POST",
        body: JSON.stringify(route),
        headers: {
          "Content-Type": "application/json",
          Authorization: getToken()
        }
      }
    );

    let confirmRouteResponseData = await confirmRouteResponse.json();

    return confirmRouteResponseData;
  } catch (error) {
    console.log("Fetch error confirmRouteFetch()", error);
  }
};

export {
  getOrders,
  getOrdersByRouteNumber,
  getRouteNumbers,
  getAllRouteOrders,
  pendingRoutesOrders,
  allPendingRoutesOrders,
  confirmPendingRouteFetch,
  confirmRouteFetch
};
