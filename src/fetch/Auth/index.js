const login = async (email, password) => {
  try {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/api/auth/login`,
      {
        method: "POST",
        body: JSON.stringify({
          email,
          password
        }),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        }
      }
    );

    const responseData = await response.json();

    if (!responseData.data) {
      return responseData;
    } else {
      const { data } = responseData;

      return data;
    }
  } catch (error) {
    console.log("FLEXIO", error);
  }
};

const register = async (registerData) => {
  try {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/api/auth/register`,
      {
        method: "POST",
        body: JSON.stringify(registerData),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        }
      }
    );

    const responseData = await response.json();

    if (!responseData.data) {
      return responseData;
    } else {
      const { data } = responseData;

      return data;
    }
  } catch (error) {
    console.log("FLEXIO", error);
  }
};

export { login, register };
