import { getToken } from "../../helpers/auth-helpers";

const getAllUsers = async () => {
  const allUsersResponse = await fetch(
    `${process.env.REACT_APP_API_URL}/api/user/allUsers`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: getToken()
      }
    }
  );
  const allUsersData = await allUsersResponse.json();

  return allUsersData;
};

const updateUserAdmin = async (userID, isAdmin) => {
  const updateUserResponse = await fetch(
    `${process.env.REACT_APP_API_URL}/api/user/updateUserAdmin`,
    {
      method: "POST",
      body: JSON.stringify({
        userID,
        isAdmin
      }),
      headers: {
        "Content-Type": "application/json",
        Authorization: getToken()
      }
    }
  );

  const updateUserData = await updateUserResponse.json();

  return updateUserData;
};

const fetchIsAdmin = async (userID) => {
  const isAdminResponse = await fetch(
    `${process.env.REACT_APP_API_URL}/api/user/isAdmin`,
    {
      method: "POST",
      body: JSON.stringify({
        userID
      }),
      headers: {
        "Content-Type": "application/json",
        Authorization: getToken()
      }
    }
  );

  const isAdminResponseData = await isAdminResponse.json();

  return isAdminResponseData.result;
};

export { getAllUsers, updateUserAdmin, fetchIsAdmin };
