const monthsES = [
  "Enero",
  "Febrero",
  "Marzo",
  "Abril",
  "Mayo",
  "Junio",
  "Julio",
  "Agosto",
  "Septiembre",
  "Octubre",
  "Noviembre",
  "Diciembre"
];

const weeksES = [
  "Domingo",
  "Lunes",
  "Martes",
  "Miércoles",
  "Jueves",
  "Viernes",
  "Sábado"
];

const flexioRates = {
  "Puerto Rico": {
    "Flexio Shift": {
      vansXL: {
        basePrice: 69.3,
        priceperMileOnRoute: 0.58,
        pricePerReturn: 0.29,
        pricePerAdditionalHour: 15.4
      },
      vansSM: {
        basePrice: 63,
        priceperMileOnRoute: 0.55,
        pricePerReturn: 0.28,
        pricePerAdditionalHour: 14
      },
      SUV: {
        basePrice: 56.7,
        priceperMileOnRoute: 0.52,
        pricePerReturn: 0.26,
        pricePerAdditionalHour: 12.6
      },
      cars: {
        basePrice: 53.55,
        priceperMileOnRoute: 0.5,
        pricePerReturn: 0.25,
        pricePerAdditionalHour: 11.9
      }
    },
    "Flexio Now": {
      SUV: {
        basePrice: 10,
        priceperMileOnRoute: 0.95,
        pricePerReturn: 0.4,
        overWaitingTime: 0.4,
        overTransitTime: 0.2
      },
      cars: {
        basePrice: 5,
        priceperMileOnRoute: 0.88,
        pricePerReturn: 0.58,
        overWaitingTime: 0.3,
        overTransitTime: 0.2
      }
    }
  },
  Florida: {
    "Flexio Shift": {
      vansXL: {
        basePrice: 78.75,
        priceperMileOnRoute: 0.58,
        pricePerReturn: 0.58,
        pricePerAdditionalHour: 17.5
      },
      vansSM: {
        basePrice: 72.45,
        priceperMileOnRoute: 0.55,
        pricePerReturn: 0.55,
        pricePerAdditionalHour: 16.1
      },
      SUV: {
        basePrice: 66.15,
        priceperMileOnRoute: 0.52,
        pricePerReturn: 0.52,
        pricePerAdditionalHour: 14.7
      },
      cars: {
        basePrice: 63.0,
        priceperMileOnRoute: 0.5,
        pricePerReturn: 0.5,
        pricePerAdditionalHour: 14.0
      }
    }
  }
};

const truckMilesPrice = {
  1: [103.21, 150.99],
  2: [105.2, 153.89],
  3: [107.27, 156.93],
  4: [109.23, 159.81],
  5: [111.28, 162.81],
  6: [113.36, 165.85],
  7: [115.47, 168.92],
  8: [117.6, 172.04],
  9: [119.74, 175.16],
  10: [121.9, 178.34],
  11: [124.08, 181.53],
  12: [126.28, 184.75],
  13: [128.5, 187.98],
  14: [130.72, 191.24],
  15: [132.96, 194.96],
  16: [135.22, 197.81],
  17: [137.49, 201.14],
  18: [139.77, 204.48],
  19: [142.06, 207.82],
  20: [144.36, 211.2],
  21: [146.66, 214.58],
  22: [148.99, 217.95],
  23: [151.32, 221.37],
  24: [153.64, 224.77],
  25: [155.97, 228.19],
  26: [158.31, 231.6],
  27: [160.65, 235.02],
  28: [162.99, 238.43],
  29: [165.33, 241.85],
  30: [167.5, 245.06],
  31: [169.99, 248.69],
  32: [172.33, 252.1],
  33: [174.66, 255.52],
  34: [176.99, 258.92],
  35: [179.3, 262.33],
  36: [181.62, 265.69],
  37: [183.92, 269.08],
  38: [186.22, 272.44],
  39: [188.51, 275.77],
  40: [190.79, 279.11],
  41: [193.05, 282.41],
  42: [195.31, 285.73],
  43: [197.56, 289.0],
  44: [199.76, 292.26],
  45: [201.98, 295.48],
  46: [204.18, 298.7],
  47: [206.35, 301.87],
  48: [208.51, 305.03],
  49: [210.65, 308.16],
  50: [212.76, 311.26],
  51: [214.84, 314.3],
  52: [216.91, 317.35],
  53: [218.97, 320.35],
  54: [221.0, 323.31],
  55: [223.0, 326.23],
  56: [224.96, 329.11],
  57: [226.92, 331.97],
  58: [228.83, 334.75],
  59: [230.71, 337.53],
  60: [232.57, 340.24],
  61: [234.4, 342.91],
  62: [236.18, 345.53],
  63: [237.94, 348.12],
  64: [239.66, 350.63],
  65: [241.35, 353.09],
  66: [243.0, 355.51],
  67: [244.62, 357.87],
  68: [246.21, 360.18],
  69: [247.73, 362.42],
  70: [249.24, 364.62],
  71: [250.7, 366.75],
  72: [252.09, 368.81],
  73: [253.4, 370.71],
  74: [254.79, 372.77],
  75: [256.08, 374.64],
  76: [257.32, 376.44],
  77: [258.49, 378.17],
  78: [259.61, 379.83],
  79: [260.73, 381.44],
  80: [261.76, 382.94],
  81: [262.75, 384.38],
  82: [263.68, 385.75],
  83: [264.56, 387.04],
  84: [265.38, 388.24],
  85: [266.13, 389.33],
  86: [266.87, 390.43],
  87: [267.53, 391.39],
  88: [268.13, 392.26],
  89: [268.67, 393.04],
  90: [269.16, 393.76],
  91: [269.56, 394.37],
  92: [269.93, 394.88],
  93: [270.21, 395.32],
  94: [270.44, 395.65],
  95: [270.6, 395.88],
  96: [270.7, 396.03],
  97: [270.74, 396.07],
  98: [270.79, 396.14],
  99: [270.83, 396.22],
  100: [270.86, 396.28],
  101: [270.89, 396.33],
  102: [270.92, 396.38],
  103: [270.95, 396.43],
  104: [270.98, 396.48],
  105: [271.01, 396.53],
  106: [271.04, 396.58],
  107: [271.07, 396.63],
  108: [271.1, 396.68],
  109: [271.13, 396.73],
  110: [271.16, 396.78],
  111: [271.19, 396.83],
  112: [271.22, 396.88],
  113: [271.25, 396.93],
  114: [271.28, 396.98],
  115: [271.31, 397.03],
  116: [271.34, 397.08],
  117: [271.37, 397.13],
  118: [271.4, 397.18],
  119: [271.43, 397.23],
  120: [271.46, 397.28],
  121: [271.49, 397.33],
  122: [271.52, 397.38],
  123: [271.55, 397.43],
  124: [271.58, 397.48],
  125: [271.61, 397.53],
  126: [271.64, 397.58],
  127: [271.67, 397.63],
  128: [271.7, 397.68],
  129: [271.73, 397.73],
  130: [271.76, 397.78],
  131: [271.79, 397.83],
  132: [271.82, 397.88],
  133: [271.85, 397.93],
  134: [271.88, 397.98],
  135: [271.91, 398.03],
  136: [271.94, 398.08],
  137: [271.97, 398.13],
  138: [272.0, 398.18],
  139: [272.03, 398.23],
  140: [272.06, 398.28],
  141: [272.09, 398.33],
  142: [272.12, 398.38],
  143: [272.15, 398.43],
  144: [272.18, 398.48],
  145: [272.21, 398.53],
  146: [272.24, 398.58],
  147: [272.27, 398.63],
  148: [272.3, 398.68],
  149: [272.33, 398.73],
  150: [272.36, 398.78],
  151: [272.39, 398.83],
  152: [272.42, 398.88],
  153: [272.45, 398.93],
  154: [272.48, 398.98],
  155: [272.51, 399.03],
  156: [272.54, 399.08],
  157: [272.57, 399.13],
  158: [272.6, 399.18],
  159: [272.63, 399.23],
  160: [272.66, 399.28],
  161: [272.69, 399.33],
  162: [272.72, 399.38],
  163: [272.75, 399.43],
  164: [272.78, 399.48],
  165: [272.81, 399.53],
  166: [272.84, 399.58],
  167: [272.87, 399.63],
  168: [272.9, 399.68],
  169: [272.93, 399.73],
  170: [272.96, 399.78],
  171: [272.99, 399.83],
  172: [273.02, 399.88],
  173: [273.05, 399.93],
  174: [273.08, 399.98],
  175: [273.11, 400.03],
  176: [273.14, 400.08],
  177: [273.17, 400.13],
  178: [273.2, 400.18],
  179: [273.23, 400.23],
  180: [273.26, 400.28],
  181: [273.29, 400.33],
  182: [273.32, 400.38],
  183: [273.35, 400.43],
  184: [273.38, 400.48],
  185: [273.41, 400.53],
  186: [273.44, 400.58],
  187: [273.47, 400.63],
  188: [273.5, 400.68],
  189: [273.53, 400.73],
  190: [273.56, 400.78],
  191: [273.59, 400.83],
  192: [273.62, 400.88],
  193: [273.65, 400.93],
  194: [273.68, 400.98],
  195: [273.71, 401.03],
  196: [273.74, 401.08],
  197: [273.77, 401.13],
  198: [273.8, 401.18]
};

const changeStatusColor = (status) => {
  switch (status.toLowerCase().trim()) {
    case "on queue":
      return "secondary";
    case "pending":
      return "secondary";
    case "started":
      return "warning";
    case "successful":
      return "success";
    case "in progress":
      return "primary";
    case "failed":
      return "danger";

    default:
      break;
  }
};

const changePickupStatusAndColor = (pickup) => {
  const lastDelivery = pickup.deliveries.pop();
  const pickupStatusAndColor = {};

  const allDeliveriesSuccessfull = pickup.deliveries.every(
    (delivery) =>
      delivery.status_name.toLowerCase() !== "on queue" &&
      delivery.status_name.toLowerCase() !== "pending"
  );

  if (allDeliveriesSuccessfull) {
    pickupStatusAndColor.status_name = "Completed";
    pickupStatusAndColor.color = "success";

    pickup.deliveries.push(lastDelivery);
    return pickupStatusAndColor;
  }

  pickup.deliveries.map((delivery) => {
    if (
      delivery.status_name.toLowerCase() === "successful" ||
      delivery.status_name.toLowerCase() === "started" ||
      delivery.status_name.toLowerCase() === "failed"
    ) {
      pickupStatusAndColor.status_name = "In Progress";
      pickupStatusAndColor.color = "primary";
    } else if (delivery.status_name.toLowerCase() === "on queue") {
      pickupStatusAndColor.status_name = "On Queue";
      pickupStatusAndColor.color = "secondary";
    } else if (delivery.status_name.toLowerCase() === "pending") {
      pickupStatusAndColor.status_name = "Pending";
      pickupStatusAndColor.color = "secondary";
    }
  });

  pickup.deliveries.push(lastDelivery);
  return pickupStatusAndColor;
};

const phoneMask = (phone) => {
  if (!phone) return "";

  return phone
    .replace(/\D/g, "")
    .replace(/^(\d)/, "($1")
    .replace(/^(\(\d{3})(\d)/, "$1) $2")
    .replace(/(\d{3})(\d{1,5})/, "$1-$2")
    .replace(/(-\d{5})\d+?$/, "$1");
};

const isNotaNumber = (number) => {
  if (isNaN(number)) {
    return false;
  }

  return true;
};

export {
  monthsES,
  weeksES,
  flexioRates,
  truckMilesPrice,
  changeStatusColor,
  phoneMask,
  changePickupStatusAndColor,
  isNotaNumber
};
