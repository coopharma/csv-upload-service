const TOKEN_KEY = "Authorization";
const USER_INFO = "user_info";

const getToken = () => {
  return localStorage.getItem(TOKEN_KEY);
};

const setToken = (APIToken) => {
  localStorage.setItem(TOKEN_KEY, APIToken);
};

const deleteToken = () => {
  localStorage.removeItem(TOKEN_KEY);
};

const getUserInfo = () => {
  return localStorage.getItem(USER_INFO);
};

const setUserInfo = (userInfo) => {
  localStorage.setItem(USER_INFO, userInfo);
};

const deleteCurrentUserInfo = () => {
  localStorage.removeItem(USER_INFO);
};

export {
  setToken,
  deleteToken,
  getToken,
  getUserInfo,
  setUserInfo,
  deleteCurrentUserInfo
};
